#ReexonLib

![061062-blue-jelly-icon-people-things-diamond5-sc27.png](https://bitbucket.org/repo/9a4R8q/images/3592773228-061062-blue-jelly-icon-people-things-diamond5-sc27.png)

# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
#### Versions
* 1.0.0

#### Logic Versions
##### X.Y.B_Z
* X = Library Version
* Y = Add Feature
* B = Bug Fix
* Z = Release for develop. Beta Version. 

### Requirements
* Java 1.8 or later. 
* [Java Cryptography Extension (JCE) Unlimited Strength Jurisdiction Policy Files 8 Download](http://www.oracle.com/technetwork/java/javase/downloads/jce8-download-2133166.html). Install the file in ${java.home}/jre/lib/security/.


#### How do I get set up? 

#### Summary of set up
#### Configuration
##### Log4j IntelliJ IDEA

* Install [Log4j Plugin](https://plugins.jetbrains.com/plugin/13?pr=)
* Then, create a run or test configuration, on VM Options just add:
```
#!

-Dlog4j.configurationFile=file:test/log4j2.xml
```

![Screen Shot 2016-07-20 at 14.43.24.png](https://bitbucket.org/repo/9a4R8q/images/27386098-Screen%20Shot%202016-07-20%20at%2014.43.24.png)

### Dependencies ###
### Database configuration ###
### How to run tests ###
### Deployment instructions ###

### Contribution guidelines ###

* Every public or protected method must be preceded by a comment.
* Every public or protected method must have to be tested.
* Using the develop branch to develop new features.
* Using a branch derived from develop to develop a specific feature.
* Using a pull-request to return the main brach.
* When all methods will be tested , accompanied by comments, you can make a pull -request to bring the changes on the stable branch.

### Code guidelines ###
* A method can have at most 50 lines.
* A class can have up to 500 lines.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact