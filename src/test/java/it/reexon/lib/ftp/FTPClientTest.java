package it.reexon.lib.ftp;

import com.github.javafaker.Faker;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPFileFilter;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockftpserver.fake.FakeFtpServer;
import org.mockftpserver.fake.UserAccount;
import org.mockftpserver.fake.filesystem.DirectoryEntry;
import org.mockftpserver.fake.filesystem.FileEntry;
import org.mockftpserver.fake.filesystem.FileSystem;
import org.mockftpserver.fake.filesystem.UnixFakeFileSystem;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


class FTPClientTest
{
    private final static String FTP_HOST = "localhost";
    private final static String FTP_USERNAME = "user";
    private final static String FTP_PASSWORD = "password";

    private FTPClient ftpClient;
    private FakeFtpServer fakeFtpServer;
    private FileSystem fileSystem;
    private Faker faker;

    @BeforeEach
    void setUp() throws Exception
    {

        faker = new Faker();
        fakeFtpServer = new FakeFtpServer();
        fakeFtpServer.setServerControlPort(0);
        fakeFtpServer.addUserAccount(new UserAccount("user", "password", "/"));

        fileSystem = new UnixFakeFileSystem();
        fileSystem.add(new DirectoryEntry("/"));

        fakeFtpServer.setFileSystem(fileSystem);
        fakeFtpServer.start();

        ftpClient = new FTPClient();
        ftpClient.connect(FTP_HOST, fakeFtpServer.getServerControlPort());
        ftpClient.login(FTP_USERNAME, FTP_PASSWORD);

    }

    /**
     *
     * @param path Path where i should generate the file
     * @param name filename to write, pass null to make the function build random filename
     * @param format format of filename (txt,mkv,avi,mp3)
     * @param quantity number of files to generate
     *
     */
    private void generateFiles(String path, String name, String format, int quantity)
    {

        if (StringUtils.isBlank(path))
        {
            throw new IllegalArgumentException("Path can't be empty");
        }

        if (!StringUtils.isBlank(name) && quantity > 1)
        {
            throw new IllegalArgumentException("In order to generate multiple file on same dir, you should not pass filename");
        }

        for (int i = 0; i < quantity; i++)
        {
            //TODO: make sure the "faker.name().firstName()", generating unique string (generating multiple file with same name will raise an error)
            String fileName = StringUtils.isBlank(name) ? faker.name().firstName() : name;
            String fileFormat = StringUtils.isBlank(format) ? "txt" : format;
            String fullpath = path + fileName + '.' + fileFormat;
            fileSystem.add(new FileEntry(fullpath));
            //fileSystem.add(new FileEntry(path + fileName + '.' + fileFormat,CONTENT_TO_WRITE));
        }
    }

    @Test
    @Disabled
    void deepSearchTXTFiles() throws Exception
    {

        generateFiles("/", null, "txt", 20);

        //Search only file with .txt format
        FTPFileFilter filter = FTPFileFilters.FORMAT(".txt");
        List<FTPFile> matches = ftpClient.deepSearch("", filter);
        assertEquals(20, matches.size());
    }

    @Test
    void deepSearchNameFiles() throws Exception
    {

        generateFiles("/", "tesdtDeadpoolsomestring", "avi", 1);
        generateFiles("/data/", "filewithDEADPOOLstring", "avi", 1);
        generateFiles("/another/dir/tree/", "filewithDEADPOOLstring", "avi", 1);
        generateFiles("/some/dir/tree/", "filewithDEADPOOLstring", "avi", 1);

        // Search for file/directory with "deadpool" in the name
        FTPFileFilter filter = FTPFileFilters.REGEXP(".*deadpool.*");
        List<FTPFile> matches = ftpClient.deepSearch("", filter);
        assertEquals(4, matches.size());
    }

    @Test
    @Disabled
    void deepSearch() throws Exception
    {}

    @AfterEach
    void tearDown() throws Exception
    {
        fakeFtpServer.stop();
    }
}