/*
  Copyright (c) 2016 Marco Velluto
 */
package it.reexon.lib.list;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


/**
 * @author Marco Velluto
 * @since 1.0
 */
class ListUtilsTest
{
    private static List<Integer> listInt3 = new ArrayList<>(3);
    private static List<Integer> listInt6 = new ArrayList<>(6);
    private static List<Integer> listInt7 = new ArrayList<>(7);

    private static List<Double> listDobule3 = new ArrayList<>(3);
    private static List<Double> listDobule6 = new ArrayList<>(6);
    private static List<Double> listDobule7 = new ArrayList<>(7);

    private static List<Long> listLong3 = new ArrayList<>(3);
    private static List<Long> listLong6 = new ArrayList<>(6);
    private static List<Long> listLong7 = new ArrayList<>(7);

    @BeforeAll
    static void setUp() throws Exception
    {
        listInt3.add(1);
        listInt3.add(2);
        listInt3.add(3);

        listInt6.add(1);
        listInt6.add(2);
        listInt6.add(3);
        listInt6.add(4);
        listInt6.add(5);
        listInt6.add(6);

        listInt7.add(1);
        listInt7.add(2);
        listInt7.add(3);
        listInt7.add(4);
        listInt7.add(5);
        listInt7.add(6);
        listInt7.add(7);

        listDobule3.add(1.5d);
        listDobule3.add(2.5d);
        listDobule3.add(3.5d);

        listDobule6.add(1.5d);
        listDobule6.add(2.5d);
        listDobule6.add(3.5d);
        listDobule6.add(4.5d);
        listDobule6.add(5.5d);
        listDobule6.add(6.5d);

        listDobule6.add(1.5d);
        listDobule6.add(2.5d);
        listDobule6.add(3.5d);
        listDobule6.add(4.5d);
        listDobule6.add(5.5d);
        listDobule6.add(6.5d);
        listDobule6.add(7.5d);

        listLong3.add(1L);
        listLong3.add(2L);
        listLong3.add(3L);

        listLong6.add(1L);
        listLong6.add(2L);
        listLong6.add(3L);
        listLong6.add(4L);
        listLong6.add(5L);
        listLong6.add(6L);

        listLong7.add(1L);
        listLong7.add(2L);
        listLong7.add(3L);
        listLong7.add(4L);
        listLong7.add(5L);
        listLong7.add(6L);
        listLong7.add(7L);
    }

    @AfterAll
    static void tearDown() throws Exception
    {
        listInt3.clear();
        listInt6.clear();
        listInt7.clear();

        listDobule3.clear();
        listDobule6.clear();
        listDobule7.clear();

        listLong3.clear();
        listLong6.clear();
        listLong7.clear();
    }

    @Test
    @Disabled
    final void testCastList()
    {/*TODO*/}

    @Test
    void testConstructorIsPrivate() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException
    {
        Constructor<ListUtils> constructor = ListUtils.class.getDeclaredConstructor();
        assertTrue(Modifier.isPrivate(constructor.getModifiers()));
        constructor.setAccessible(true);
        constructor.newInstance();
    }

    @Test
    void testFindEverageElementInt()
    {
        Integer everageElement3 = ListUtils.findAverageElementInt(listInt3);
        assertNotNull(everageElement3);
        assertEquals(2, everageElement3.intValue());

        Integer everageElement6 = ListUtils.findAverageElementInt(listInt6);
        assertNotNull(everageElement6);
        assertEquals(3, everageElement6.intValue());

        Integer everageElement7 = ListUtils.findAverageElementInt(listInt7);
        assertNotNull(everageElement7);
        assertEquals(4, everageElement7.intValue());

        assertNull(ListUtils.findAverageElementInt(Collections.emptyList()));
        assertNull(ListUtils.findAverageElementInt(new LinkedList<>()));
    }

    @Test
    @Disabled
    final void testFindEverageElementLong()
    {
        Long everageElement3 = ListUtils.findAverageElementLong(listLong3);
        assertEquals(2L, everageElement3.longValue());

        Long everageElement6 = ListUtils.findAverageElementLong(listLong6);
        assertEquals(3L, everageElement6.longValue());

        Long everageElement7 = ListUtils.findAverageElementLong(listLong7);
        assertEquals(4L, everageElement7.longValue());

        assertNull(ListUtils.findAverageElementLong(Collections.emptyList()));
        assertNull(ListUtils.findAverageElementLong(new LinkedList<>()));
    }

    @Test
    @Disabled
    final void orderByElementLongTest()
    {
        List<Long> listSort3 = ListUtils.orderByElementLong(listLong3, 3L);
        List<Long> listSort3Expected = ListUtils.createList(3L, 2L, 1L);
        assertArrayEquals(listSort3Expected.toArray(), listSort3.toArray());

        listSort3 = ListUtils.orderByElementLong(listLong3, 4L);
        listSort3Expected = ListUtils.createList(3L, 2L, 1L);
        assertArrayEquals(listSort3Expected.toArray(), listSort3.toArray());

        List<Long> listSort6 = ListUtils.orderByElementLong(listLong6, 3L); //1-2-3-4-5-6
        List<Long> listSort6Expected = ListUtils.createList(3L, 2L, 4L, 1L, 5L, 6L);
        assertArrayEquals(listSort6Expected.toArray(), listSort6.toArray());

        List<Long> listSort7 = ListUtils.orderByElementLong(listLong7, 4L); //1-2-3-4-5-6-7
        List<Long> listSort7Expected = ListUtils.createList(4L, 3L, 5L, 2L, 6L, 1L, 7L);
        assertArrayEquals(listSort7Expected.toArray(), listSort7.toArray());

        assertNull(ListUtils.orderByElementLong(Collections.emptyList(), 1L));
    }

    @Test
    @Disabled
    void nearestElementTest()
    {
        Long longValueFor1 = ListUtils.nearestElement(listLong7, -3L);
        assertEquals(1, longValueFor1.longValue());

        Long longValueFor0 = ListUtils.nearestElement(listLong7, 0L);
        assertEquals(1, longValueFor0.longValue());

        Long longValueFor3 = ListUtils.nearestElement(listLong7, 3L);
        assertEquals(3, longValueFor3.longValue());

        Long longValueFor9 = ListUtils.nearestElement(listLong7, 9L);
        assertEquals(7, longValueFor9.longValue());

        assertNull(ListUtils.nearestElement(Collections.emptyList(), 1L));
    }

    @Test
    void createList()
    {
        List<Long> longList = ListUtils.createList(1L, 2L, 3L);
        assertArrayEquals(listLong3.toArray(), longList.toArray());

        List<Long> nullList = ListUtils.createList(null, null);
        assertEquals(nullList.toString(), "[null, null]");

        List<String> strList = ListUtils.createList("s1", "s2", "s3");
        assertEquals(strList.toString(), "[s1, s2, s3]");
    }
}
