/*
  Copyright (c) 2016 Marco Velluto. All rights reserved.
 */
package it.reexon.lib.file;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;


/**
 * @author Marco Velluto
 */
class IOUtilsTest
{
    @Test
    @Disabled
    void testImportFiles()
    {
    }

    @Test
    @Disabled
    void testImportFilesWithFileBoolean()
    {
    }

    @Test
    @Disabled
    void testMoveFile()
    {
    }

    @Test
    @Disabled
    void testWriteLines()
    {
    }

}
