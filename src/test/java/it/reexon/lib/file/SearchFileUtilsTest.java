package it.reexon.lib.file;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertTrue;


/**
 * Created by Marco Velluto on 07/04/2017.
 *
 */
class SearchFileUtilsTest
{

    @Test
    void testConstructorIsPrivate() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException
    {
        Constructor<SearchFileUtils> constructor = SearchFileUtils.class.getDeclaredConstructor();
        assertTrue(Modifier.isPrivate(constructor.getModifiers()));
        constructor.setAccessible(true);
        constructor.newInstance();
    }

    @Test
    @Disabled
    void searchFor() throws IOException
    {
        Path path = Paths.get("");
        SearchFileUtils.searchFor("", path);
    }

    @Test
    @Disabled
    void exists() throws IOException
    {
        Path path = Paths.get("");
        SearchFileUtils.exists("", path);
    }

}