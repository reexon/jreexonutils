package it.reexon.lib.file;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertTrue;


/**
 * Created by Marco Velluto on 26/06/2017.
 *
 */
class CheckFilesUtilsTest
{

    @Test
    void testConstructorIsPrivate() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException
    {
        Constructor<CheckFilesUtils> constructor = CheckFilesUtils.class.getDeclaredConstructor();
        assertTrue(Modifier.isPrivate(constructor.getModifiers()));
        constructor.setAccessible(true);
        constructor.newInstance();
    }

    @Test
    @Disabled
    void checkEqualsFiles() throws IOException, NoSuchAlgorithmException
    {
        CheckFilesUtils.checkEqualsFiles(new File(""), new File(""), "");
    }

    @Test
    void checkEqualsDirectories() throws IOException, NoSuchAlgorithmException
    {
        CheckFilesUtils.checkEqualsDirectories(new File(""), new File(""));
    }

    @Test
    void checkFilesForName()
    {
        CheckFilesUtils.checkFilesForName(Collections.emptyList(), Collections.emptyList());
    }

}