/*
  Copyright (c) 2016 Marco Velluto. All rights reserved.
 */
package it.reexon.lib.file;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.security.NoSuchAlgorithmException;

import static java.time.Duration.ofSeconds;
import static org.junit.jupiter.api.Assertions.*;


/**
 * @author Marco Velluto
 */
class FileUtilsTest extends AbstractFileTest
{
    private static final Logger logger = LogManager.getLogger(FileUtilsTest.class);
    private static File newDirecoty = new File(DIRECTORY.getPath() + "_new");
    private static File newDirectory_1 = new File(DIRECTORY.getPath() + "_testDeleteDirectoryFile_1");
    private static File newDirectory_2 = new File(DIRECTORY.getPath() + "_testDeleteDirectoryFile_2");

    @AfterAll
    static void tearDownAfterClass() throws Exception
    {
        FileUtils.forceDelete(DIRECTORY);
        FileUtils.forceDelete(DIRECTORY_2);
        FileUtils.forceDelete(DIRECTORY_3);
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @BeforeEach
    void before()
    {
        newDirecoty.deleteOnExit();
        newDirecoty.mkdir();

        newDirectory_1.deleteOnExit();
        newDirectory_1.mkdirs();

        newDirectory_2.deleteOnExit();
        newDirectory_2.mkdirs();
    }

    @Test
    @Disabled
    void bitmapToImage() throws Exception
    {
        assertTimeoutPreemptively(ofSeconds(2), () -> FileUtils.bitmapToImage(new FileInputStream(""), ""));
    }

    @Test
    @Disabled
    void checkEqualDirecoty()
    {
        assertAll("directories equals", () -> assertTrue(FileUtils.checkEqualDirectory(DIRECTORY, DIRECTORY_2)),
                  () -> assertTrue(FileUtils.checkEqualDirectory(DIRECTORY_2, DIRECTORY)),
                  () -> assertFalse(FileUtils.checkEqualDirectory(DIRECTORY, DIRECTORY_3)),
                  () -> assertFalse(FileUtils.checkEqualDirectory(DIRECTORY_3, DIRECTORY)));
    }

    @Test
    @Disabled
    void checkEqualDirecotyWithException()
    {
        assertThrows(IllegalArgumentException.class, () -> FileUtils.checkEqualDirectory(DIRECTORY, DIRECTORY));
    }

    @Test
    @Disabled
    void testCheckEqualFiles()
    {
        assertAll("check files", () -> assertTrue(FileUtils.checkEqualFiles(fileA1, fileA1)),
                  () -> assertTrue(FileUtils.checkEqualFiles(fileA1, fileB1)));
    }

    @Test
    @Disabled
    void testCheckEqualFilesWithException()
    {
        assertThrows(IllegalArgumentException.class, () -> FileUtils.checkEqualFiles(fileB1, new File("")));
    }

    @Test
    @Disabled
    void copyDirectory() throws IOException, NoSuchAlgorithmException
    {
        assertTrue(CheckFilesUtils.checkEqualsDirectories(DIRECTORY, newDirecoty));
    }

    @Test
    void copyDirectoryWithException() throws IOException, NoSuchAlgorithmException
    {
        assertThrows(IllegalArgumentException.class, () -> FileUtils.copyDirectory(new File(""), DIRECTORY));
    }

    @Test
    void copyFile()
    {
        File destFile = new File(DIRECTORY.getPath() + "/" + fileA.getName());
        try
        {
            FileUtils.copyFile(fileA, destFile);
            Boolean isEquals = FileUtils.checkEqualFiles(fileA, destFile);
            assertTrue(isEquals);

            try
            {
                FileUtils.copyFile(new File(""), fileA);
                fail("Should have thrown an exception");
            }
            catch (Exception e)
            {
                assertEquals(e.getClass(), java.io.FileNotFoundException.class);
            }
        }
        catch (Exception e)
        {
            logger.error(e);
            throw new RuntimeException(e);
        }
        finally
        {
            try
            {
                FileUtils.forceDelete(destFile);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    @Test
    @Disabled
    void testDeleteDirectoryFile() throws IOException
    {
        assertTrue(newDirectory_1.mkdirs(), "Directory must exists");

        //Delete empty directory
        FileUtils.deleteDirectory(newDirectory_1);
        assertAll("Delete empty Directory", () -> assertFalse(newDirectory_1.exists(), "The Directory should no longer exist"));

        //Delete not empty directory
        assertAll("Delete not empty Directory", () -> assertTrue(newDirectory_2.mkdirs(), "Directory must exists"),
                  () -> assertFalse(newDirectory_1.exists(), "The Directory should no longer exist"));
    }

    @Test
    void testDeleteDirectoryFileWithException() throws IOException
    {
        assertThrows(FileNotFoundException.class, () -> FileUtils.deleteDirectory(new File("")));
    }

    @Test
    void forceDelete() throws IOException
    {
        File file = new File("testForceDelete_FILE");
        FileUtils.copyFile(fileA1, file);
        assertTrue(file.exists(), "File must exists");

        FileUtils.forceDelete(file);
        assertFalse(file.exists(), "The File should no longer exist");
    }

    @Test
    void forceDeleteWithFileNotFoundException() throws IOException
    {
        assertThrows(FileNotFoundException.class, () -> FileUtils.forceDelete(new File("")));
    }

    @Test
    @Disabled
    void getByteFromFile() throws IOException
    {
        FileUtils.getByteFromFile(new File(""));
    }

    @Test
    @Disabled
    void getScaledInstance()
    {
        FileUtils.getScaledInstance(new BufferedImage(0, 0, 0), 0, 0, 0, true);
    }

    @Test
    @Disabled
    void imageToBitmap() throws IOException
    {
        FileUtils.imageToBitmap(new BufferedImage(0, 0, 0), "", "");
    }

    @Test
    void checkEqualDirectory() throws Exception
    {
        FileUtils.checkEqualDirectory(new File(""), new File(""));
    }

    @Test
    @Disabled
    void checkEqualFiles() throws Exception
    {
        FileUtils.checkEqualFiles(new File(""), new File(""));
    }

    @Test
    @Disabled
    void deleteDirectory() throws Exception
    {
        FileUtils.deleteDirectory(new File(""));
    }

    @Test
    void deleteFile() throws Exception
    {
        FileUtils.deleteFile(FileSystems.getDefault().getPath(""));
    }

    @Test
    void moveFile() throws Exception
    {
        FileUtils.moveFile(FileSystems.getDefault().getPath(""), FileSystems.getDefault().getPath(""));
    }
}
