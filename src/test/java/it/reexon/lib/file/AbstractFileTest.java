package it.reexon.lib.file;

import it.reexon.lib.list.ListUtils;
import org.junit.jupiter.api.BeforeAll;

import java.io.File;
import java.util.LinkedList;
import java.util.List;


/**
 * Created by Marco Velluto on 10/04/2017.
 *
 */
public abstract class AbstractFileTest
{
    protected static final File DIRECTORY = new File("test/FileUtilsTest");
    protected static final File DIRECTORY_2 = new File("test/checkFileUtils_2");
    protected static final File DIRECTORY_3 = new File("test/checkFileUtils_3");

    protected static File fileA = null;
    protected static File fileA1 = null;
    protected static File fileB = null;
    protected static File fileB1 = null;
    protected static File fileB4 = null;
    protected static File fileB5 = null;

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @BeforeAll
    public static void setUpBeforeClass() throws Exception
    {
        if (!DIRECTORY.exists())
        {
            DIRECTORY.mkdirs();
        }

        fileA = File.createTempFile("fileA", ".txt", DIRECTORY);
        List<String> linesA = new LinkedList<>(ListUtils.createList("CiaoA, Ciao1"));
        IOUtils.writeLines(fileA, linesA);
        fileA1 = File.createTempFile("fileA1", ".txt", DIRECTORY);
        IOUtils.writeLines(fileA1, linesA);

        fileB = File.createTempFile("fileB", ".txt", DIRECTORY);
        List<String> linesB = new LinkedList<>(ListUtils.createList("CiaoB, CiaoB"));
        IOUtils.writeLines(fileB, linesB);
        fileB1 = File.createTempFile("fileB1", ".txt", DIRECTORY);
        IOUtils.writeLines(fileB1, linesB);

        FileUtils.copyDirectory(DIRECTORY, DIRECTORY_2);

        if (!DIRECTORY_3.exists())
        {
            DIRECTORY_3.mkdirs();
        }

        fileB4 = File.createTempFile("fileB", ".txt", DIRECTORY_3);
        List<String> linesB3 = new LinkedList<>(ListUtils.createList("CiaoB3, CiaoB3"));
        IOUtils.writeLines(fileB4, linesB3);
        fileB5 = File.createTempFile("fileB3", ".txt", DIRECTORY_3);
        IOUtils.writeLines(fileB5, linesB);
    }

}
