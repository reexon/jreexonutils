/*
  Copyright (c) 2016 Marco Velluto. All rights reserved.
 */
package it.reexon.lib.file.zip;

import it.reexon.lib.file.FileUtils;
import it.reexon.lib.file.IOUtils;
import it.reexon.lib.list.ListUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import static org.eclipse.persistence.jpa.jpql.Assert.fail;
import static org.junit.jupiter.api.Assertions.*;


/**
 * @author Marco Velluto
 */
@SuppressWarnings("ResultOfMethodCallIgnored")
@Disabled
class ZipUtilsTest
{
    private static Logger logger = LogManager.getLogger(ZipUtilsTest.class);

    private static File MAIN_DIRECTORY = new File("resources/tests/ZipUtilTest");

    private static File fileA = null;
    private static File fileB = null;
    private static File fileC = null;

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @BeforeAll
    static void setUpBeforeClass() throws Exception
    {
        if (!MAIN_DIRECTORY.exists())
            MAIN_DIRECTORY.mkdir();

        creaFileA();
        creaFileB();
        creaFileC();
    }

    @AfterAll
    static void tearDownAfterClass() throws Exception
    {
        try
        {
            if (MAIN_DIRECTORY.exists())
                FileUtils.forceDelete(MAIN_DIRECTORY);
        }
        catch (Exception e)
        {
            logger.error(e.getMessage());
        }
    }

    private static void creaFileA() throws IOException
    {
        fileA = File.createTempFile("fileA", ".txt", MAIN_DIRECTORY);
        List<String> linesA = new LinkedList<>(ListUtils.createList("CiaoA, CiaoB1"));
        IOUtils.writeLines(fileA, linesA);
    }

    private static void creaFileB() throws IOException
    {
        fileB = File.createTempFile("fileB", ".txt", MAIN_DIRECTORY);
        List<String> linesB = new LinkedList<>(ListUtils.createList("CiaoB, CiaoB1"));
        IOUtils.writeLines(fileB, linesB);
    }

    private static void creaFileC() throws IOException
    {
        fileC = File.createTempFile("fileC", ".txt", MAIN_DIRECTORY);
        List<String> linesC = new LinkedList<>(ListUtils.createList("CiaoC, CiaoC1"));
        IOUtils.writeLines(fileC, linesC);
    }

    @BeforeAll
    static void setUp() throws Exception
    {
        if (fileA == null || !fileA.exists())
            creaFileA();

        if (fileB == null || !fileB.exists())
            creaFileB();

        if (fileB == null || !fileB.exists())
            creaFileB();
    }

    @Test
    void testConstructorIsPrivate() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException
    {
        Constructor<ZipUtils> constructor = ZipUtils.class.getDeclaredConstructor();
        assertTrue(Modifier.isPrivate(constructor.getModifiers()));
        constructor.setAccessible(true);
        constructor.newInstance();
    }

    @Test
    void createZipOfDirectory() throws Exception
    {

        File treeDirectory = new File(MAIN_DIRECTORY + "/treedir/some/dir/file.txt");
        treeDirectory.getParentFile().mkdirs();
        treeDirectory.createNewFile();
        File second = new File(MAIN_DIRECTORY + "/treedir/some/test2.txt");
        second.createNewFile();

        File zipDirectory = new File(MAIN_DIRECTORY + "/zipdirectory.zip");
        File toZip = new File(MAIN_DIRECTORY + "/treedir");
        ZipUtils.createZipFileOfDirectory(toZip, zipDirectory);

        //Loop and check ZIP ENTRY
        try (ZipFile file = new ZipFile(MAIN_DIRECTORY + "/zipdirectory.zip"))
        {
            Enumeration<? extends ZipEntry> entries = file.entries();
            int totalFound = 0;
            while (entries.hasMoreElements())
            {
                ZipEntry entry = entries.nextElement();
                if (entry.getName().equals("some/dir/file.txt") || entry.getName().equals("some/test2.txt"))
                    totalFound++;
            }
            assertEquals(2, totalFound);
        }

    }

    @Test
    void createZipOutputStreamListOfFile() throws Exception
    {
        List<File> files = new ArrayList<>();

        try (OutputStream output = new FileOutputStream(MAIN_DIRECTORY + "/archive.zip"))
        {
            files.add(fileA);
            files.add(fileB);
            files.add(fileC);

            ZipUtils.createZip(files, output);
            File archive = new File(MAIN_DIRECTORY + "/archive.zip");
            assertTrue(archive.exists());
        }

        try (ZipFile zipFile = new ZipFile(MAIN_DIRECTORY + "/archive.zip"))
        {
            //just checking number of file present in archive.zip

            assertEquals(files.size(), zipFile.size());

            //check if zipped files are the expected ones
            List<String> fileNames = files.stream().map(File::getName).collect(Collectors.toList());

            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            while (entries.hasMoreElements())
            {
                ZipEntry entry = entries.nextElement();

                //check if current entry is expected to be zipped
                boolean included = fileNames.contains(entry.getName());

                assertTrue(included, "the file " + entry.getName() + " should not be zipped !");
            }
        }
    }

    @Test
    void createZipFileOutputStream() throws Exception
    {
        try (OutputStream os = new FileOutputStream(MAIN_DIRECTORY + "/fileOutStream.zip"))
        {
            ZipUtils.createZip(fileA, os);

            //Loop and check ZIP ENTRY
            try (ZipFile file = new ZipFile(MAIN_DIRECTORY + "/fileOutStream.zip"))
            {
                Enumeration<? extends ZipEntry> entries = file.entries();
                while (entries.hasMoreElements())
                {
                    ZipEntry entry = entries.nextElement();
                    assertTrue(entry.getName().equals(fileA.getName()), "fileOutStream.zip has zipped wrong file.");
                }
            }
        }
    }

    @Test
    void createZipFileZipOutputStream() throws Exception
    {

        try (OutputStream os = new FileOutputStream(MAIN_DIRECTORY + "/zipOutStream.zip"); ZipOutputStream zipOutputStream = new ZipOutputStream(os))
        {
            ZipUtils.createZip(fileA, zipOutputStream);

            //Loop and check ZIP ENTRY
            try (ZipFile file = new ZipFile(MAIN_DIRECTORY + "/zipOutStream.zip"))
            {
                Enumeration<? extends ZipEntry> entries = file.entries();
                while (entries.hasMoreElements())
                {
                    ZipEntry entry = entries.nextElement();
                    assertTrue(entry.getName().equals(fileA.getName()), "zipOutStream.zip has zipped wrong file.");
                }
            }
        }
    }

    @Test
    void createZipFileOfDirectory() throws IOException
    {
        File[] filesToZip = MAIN_DIRECTORY.listFiles();
        File zip = new File(MAIN_DIRECTORY.getAbsolutePath() + "/Zipped.zip");
        ZipUtils.createZipFileOfDirectory(MAIN_DIRECTORY, zip);

        File unzipped = new File(MAIN_DIRECTORY.getAbsolutePath() + "/unzipped");
        ZipUtils.unzip(new FileInputStream(zip), unzipped);
        File[] filesZipped = unzipped.listFiles();

        assertNotNull(filesToZip);
        assertNotNull(filesZipped);

        if (filesToZip.length != filesZipped.length)
        {
            Arrays.stream(filesZipped).map(File::getName).forEach(System.out::println);
            fail("File to zip are: " + filesToZip.length + " but file zipped are: " + filesZipped.length);
        }

            /*
             * Check if files zipped have the same name as expected ones
             */
        List<String> fileZippedNames = Arrays.stream(filesZipped).map(File::getName).collect(Collectors.toList());
        List<String> fileToZipNames = Arrays.stream(filesZipped).map(File::getName).collect(Collectors.toList());
        assertTrue(fileZippedNames.containsAll(fileToZipNames), "Some expected files are missing");

    }

    @Test
    void unzip() throws Exception
    {
        List<File> filesZipped = ListUtils.createList(fileA, fileB, fileC);
        File zip = new File(MAIN_DIRECTORY.getAbsolutePath() + "/Zipped.zip");
        zip.createNewFile();

        try (InputStream input = new FileInputStream(MAIN_DIRECTORY.getAbsolutePath() + "/Zipped.zip"))
        {
            //make zip to unzip later on
            ZipUtils.createZipFileOfDirectory(MAIN_DIRECTORY, zip);

            //unzip directory
            File output = new File(MAIN_DIRECTORY.getAbsolutePath() + "/testUnzip");
            output.mkdirs();
            ZipUtils.unzip(input, output);

            //check if extracted files are the same of zipped ones
            File[] array = output.listFiles();
            assertNotNull(array);
            List<String> extractedFiles = Arrays.stream(array).map(File::getName).collect(Collectors.toList());
            for (File fileZipped : filesZipped)
            {
                assertTrue(extractedFiles.contains(fileZipped.getName()), " File " + fileZipped.getName() + " does not exist on extracted files");
            }
        }
    }

    @Test
    @Disabled
    void addDirToZip() throws Exception
    {
    }
}
