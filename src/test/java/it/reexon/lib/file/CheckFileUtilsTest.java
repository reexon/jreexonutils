/**
 * Copyright (c) 2016 Marco Velluto. All rights reserved.
 */
package it.reexon.lib.file;

import it.reexon.lib.secure.algorithmics.MessageDigestAlgorithms;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;


/**
 * @author Marco Velluto
 */
class CheckFileUtilsTest extends AbstractFileTest
{
    @AfterAll
    static void tearDownAfterClass() throws Exception
    {
        FileUtils.forceDelete(DIRECTORY);
        FileUtils.forceDelete(DIRECTORY_2);
        FileUtils.forceDelete(DIRECTORY_3);
    }

    private static void testEqualsFile(File file1, File file2, String algorithm) throws IOException, NoSuchAlgorithmException
    {
        Boolean isEquals = CheckFilesUtils.checkEqualsFiles(file1, file1, algorithm);
        assertTrue(isEquals);

        isEquals = CheckFilesUtils.checkEqualsFiles(file1, file1, algorithm);
        assertTrue(isEquals);

        isEquals = CheckFilesUtils.checkEqualsFiles(file1, file2, algorithm);
        assertFalse(isEquals);

        isEquals = CheckFilesUtils.checkEqualsFiles(file2, file1, algorithm);
        assertFalse(isEquals);

        testEqualsFileExceptions(file1, file2, algorithm);
    }

    private static void testEqualsFileExceptions(File file1, File file2, String algorithm)
    {
        try
        {
            CheckFilesUtils.checkEqualsFiles(new File(""), file1, algorithm);
            fail("Should have thrown an exception");
        }
        catch (Exception e)
        {
            assertEquals(FileNotFoundException.class, e.getClass());
        }
    }

    private static void testCheckEqualsDirectoriesWithException()
    {
        try
        {
            CheckFilesUtils.checkEqualsDirectories(DIRECTORY, DIRECTORY);
            fail("Should have thrown an exception");
        }
        catch (Exception e)
        {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    void checkEqualsFiles() throws IOException, NoSuchAlgorithmException
    {
        for (String algorithm : MessageDigestAlgorithms.values())
            testEqualsFile(fileA1, fileB1, algorithm);
    }

    @Test
    void testCheckEqualsDirectories() throws Exception
    {
        for (String algorithm : MessageDigestAlgorithms.values())
        {
            Boolean isEquals = CheckFilesUtils.checkEqualsDirectories(DIRECTORY, DIRECTORY_2);
            assertTrue(isEquals);

            isEquals = CheckFilesUtils.checkEqualsDirectories(DIRECTORY_2, DIRECTORY);
            assertTrue(isEquals);

            isEquals = CheckFilesUtils.checkEqualsDirectories(DIRECTORY, DIRECTORY_3);
            assertFalse(isEquals);

            isEquals = CheckFilesUtils.checkEqualsDirectories(DIRECTORY_3, DIRECTORY);
            assertFalse(isEquals);
        }
    }

    @Test
    @Disabled
    void testCheckFilesForName() throws Exception
    {
        boolean isEquals = CheckFilesUtils.checkFilesForName(Arrays.asList(DIRECTORY.listFiles()), Arrays.asList(DIRECTORY_2.listFiles()));
        assertTrue(isEquals);
        isEquals = CheckFilesUtils.checkFilesForName(Arrays.asList(DIRECTORY.listFiles()), Arrays.asList(DIRECTORY_3.listFiles()));
        assertFalse(isEquals);
        File dir1 = new File("C:\\Users\\marco.velluto\\Downloads");
        File dir2 = new File("C:\\Users\\marco.velluto\\Documents\\progetti");
        isEquals = CheckFilesUtils.checkFilesForName(Arrays.asList(dir1.listFiles()), Arrays.asList(dir2.listFiles()));
        assertFalse(isEquals);
    }

}
