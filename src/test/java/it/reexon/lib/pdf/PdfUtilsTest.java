/*
  Copyright (c) 2016 Marco Velluto. All rights reserved.
 */
package it.reexon.lib.pdf;

import it.reexon.lib.file.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.encryption.BadSecurityHandlerException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.eclipse.persistence.jpa.jpql.Assert.fail;
import static org.junit.jupiter.api.Assertions.*;


/**
 * @author Marco Velluto
 * @since 1.0
 */
@SuppressWarnings("ResultOfMethodCallIgnored")
class PdfUtilsTest
{
    private static final Logger logger = LogManager.getLogger(PdfUtilsTest.class);

    private static final String PASSWORD = "Passw0rD!";
    private static final String OWNER_PASSWORD = "OWNER_Passw0rD!";

    private static final File DIRECTORY = new File("resources/tests/PdfUtilsTest");

    private static final File PDF = new File("resources/tests/pdf.pdf");

    private static final File SRC_ENCRYPT = new File(DIRECTORY + "/pdf.pdf");
    private static final File DST_ENCRYPT = new File(DIRECTORY + "/pdf_crypt.pdf");

    private static final File SRC_DECRYPT = new File(DIRECTORY + "/pdf_1_crypt.pdf");
    private static final File DST_DECRYPT = new File(DIRECTORY + "/pdf_1.pdf");

    @AfterAll
    static void tearDownAfterClass() throws Exception
    {
        FileUtils.forceDelete(DIRECTORY);
    }

    @BeforeAll
    static void setUpBeforeClass() throws Exception
    {
        PDPage blankPage = new PDPage(); //TODO: Create a complex page

        if (!DIRECTORY.exists())
            DIRECTORY.mkdirs();

        PDDocument srcEncrypt = new PDDocument();
        srcEncrypt.addPage(blankPage);
        srcEncrypt.save(SRC_ENCRYPT);
        srcEncrypt.close();

        PDDocument srcDecrypt = new PDDocument();
        srcDecrypt.addPage(blankPage);
        srcDecrypt.encrypt(OWNER_PASSWORD, PASSWORD);
        srcDecrypt.save(SRC_DECRYPT);
        srcDecrypt.close();
    }

    @Test
    @Disabled
    final void testExtractImages() throws IOException
    {
        File images = new File(DIRECTORY + "/Images");
        PDFUtils.extractImages(PDF, images);

        List<File> imagesList = Arrays.asList(images.listFiles());
        assertEquals(5, imagesList.size());
    }

    @Test
    @Disabled
    final void testEncryptPdf() throws Exception
    {
        PDFUtils.encryptPdf(SRC_ENCRYPT.getPath(), DST_ENCRYPT.getPath(), PASSWORD);
        boolean isEncrypted = PDFUtils.isEncrypted(DST_ENCRYPT.getPath());
        assertTrue(isEncrypted);

        try
        {
            PDFUtils.encryptPdf("", DST_ENCRYPT.getPath(), PASSWORD);
            fail("Should have thrown an exception");
        }
        catch (Exception e)
        {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
        try
        {
            PDFUtils.encryptPdf(" ", DST_ENCRYPT.getPath(), PASSWORD);
            fail("Should have thrown an exception");
        }
        catch (Exception e)
        {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
        try
        {
            PDFUtils.encryptPdf(SRC_ENCRYPT.getPath(), DST_ENCRYPT.getPath(), "");
            fail("Should have thrown an exception");
        }
        catch (Exception e)
        {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
        try
        {
            PDFUtils.encryptPdf(SRC_ENCRYPT.getPath(), DST_ENCRYPT.getPath(), " ");
            isEncrypted = PDFUtils.isEncrypted(DST_ENCRYPT.getPath());
            assertTrue(isEncrypted);
        }
        catch (Exception e)
        {
            logger.error(e);
            fail("Should havent thrown an exception");
        }
    }

    @Test
    @Disabled
    final void testDecrypt() throws Exception
    {

        PDFUtils.decrypt(SRC_DECRYPT.getPath(), DST_DECRYPT.getPath(), PASSWORD);
        boolean isEncrypted = PDFUtils.isEncrypted(DST_DECRYPT.getPath());
        assertFalse(isEncrypted);
        try
        {
            PDFUtils.decrypt("", DST_ENCRYPT.getPath(), PASSWORD);
            fail("Should have thrown an exception");
        }
        catch (Exception e)
        {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
        try
        {
            PDFUtils.decrypt(" ", DST_ENCRYPT.getPath(), PASSWORD);
            fail("Should have thrown an exception");
        }
        catch (Exception e)
        {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
        try
        {
            PDFUtils.decrypt(SRC_ENCRYPT.getPath(), DST_ENCRYPT.getPath(), "");
            fail("Should have thrown an exception");
        }
        catch (Exception e)
        {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
        try
        {
            PDFUtils.decrypt(SRC_ENCRYPT.getPath(), DST_ENCRYPT.getPath(), " ");
        }
        catch (Exception e)
        {
            logger.error(e);
            fail("Should havent thrown an exception");
        }
    }

    @Test
    @Disabled
    final void testSetAccessPermissionOnlyPrint() throws COSVisitorException, BadSecurityHandlerException, IOException
    {
        File pdfDest = new File(DIRECTORY + "/" + "PDF_OLY_PRINT.pdf");
        PDFUtils.setAccessPermissionOnlyPrint(PDF, pdfDest, "");
        //TODO: Verificare se effettivamente lascia solo il permesso di stampa.
        //Io ho verificato aprendo a mano il file. Realizzarlo in modo automatico.
    }

    @Test
    @Disabled
    final void testIsEncrypted() throws Exception
    {
        try
        {
            PDFUtils.encryptPdf(SRC_ENCRYPT.getPath(), DST_ENCRYPT.getPath(), PASSWORD);
            boolean isEncrypted = PDFUtils.isEncrypted(DST_ENCRYPT.getPath());
            assertTrue(isEncrypted);

            isEncrypted = PDFUtils.isEncrypted(SRC_ENCRYPT.getPath());
            assertFalse(isEncrypted);

            try
            {
                PDFUtils.isEncrypted("");
                fail("Should have thrown an exception");
            }
            catch (Exception e)
            {
                assertEquals(IllegalArgumentException.class, e.getClass());
            }
            try
            {
                PDFUtils.isEncrypted(" ");
                fail("Should have thrown an exception");
            }
            catch (Exception e)
            {
                assertEquals(IllegalArgumentException.class, e.getClass());
            }
        }
        catch (Exception e)
        {
            logger.error(e);
            throw e;
        }
    }

}
