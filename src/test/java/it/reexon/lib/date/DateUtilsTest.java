package it.reexon.lib.date;

import it.reexon.lib.list.ListUtils;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;


/**
 * Created by Marco Velluto on 12/06/2017.
 *
 */
@SuppressWarnings("SameParameterValue")
class DateUtilsTest
{
    private static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    private static Date date01;
    private static Date date02;
    private static Date date03;
    private static Date date04;
    private static Date date05;
    private static Date date10;

    @NotNull
    private static Date getDate(int year, int month, int day)
    {
        if (year < 0 || month < 0 || day < 0)
        {
            throw new IllegalArgumentException("Values must be greater of 0");
        }

        final Calendar c = Calendar.getInstance();
        c.set(year, month, day);

        return DateUtils.setTimeToBeginningOfDay(c).getTime();
    }

    @BeforeAll
    static void setUp() throws Exception
    {
        date01 = new SimpleDateFormat(YYYY_MM_DD_HH_MM_SS).parse("2016-03-01 00:00:000");
        date02 = new SimpleDateFormat(YYYY_MM_DD_HH_MM_SS).parse("2016-03-02 00:00:000");
        date03 = new SimpleDateFormat(YYYY_MM_DD_HH_MM_SS).parse("2016-03-03 00:00:000");
        date04 = new SimpleDateFormat(YYYY_MM_DD_HH_MM_SS).parse("2016-03-04 00:00:000");
        date05 = new SimpleDateFormat(YYYY_MM_DD_HH_MM_SS).parse("2016-03-05 00:00:000");
        date10 = new SimpleDateFormat(YYYY_MM_DD_HH_MM_SS).parse("2016-03-10 00:00:000");
    }

    @Test
    void testConstructorIsPrivate() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException
    {
        Constructor<DateUtils> constructor = DateUtils.class.getDeclaredConstructor();
        assertTrue(Modifier.isPrivate(constructor.getModifiers()));
        constructor.setAccessible(true);
        constructor.newInstance();
    }

    @Test
    final void addDays()
    {
        Date dateReturned = DateUtils.addDays(date01, 2);
        assertEquals(date03.getTime(), dateReturned.getTime());

        dateReturned = DateUtils.addDays(date10, -5);
        assertEquals(date05.getTime(), dateReturned.getTime());
    }

    @Test
    final void addOneDay()
    {
        Date dateReturned = DateUtils.addOneDay(date01);
        assertEquals(date02.getTime(), dateReturned.getTime());
    }

    @Test
    final void between()
    {
        Boolean between = DateUtils.between(date05, date01, date10);
        assertTrue(between);

        between = DateUtils.between(date01, date05, date10);
        assertFalse(between);
    }

    @Test
    final void betweenWithException()
    {
        assertThrows(IllegalArgumentException.class, () -> DateUtils.between(date05, date10, date01));
    }

    @Test
    final void getDatesBetween()
    {
        List<Date> listOfDate = DateUtils.getDatesBetween(date01, date10);
        assertNotNull(listOfDate);
        assertEquals(10, listOfDate.size());
        assertTrue(listOfDate.contains(getDate(2016, 2, 1)));
        assertTrue(listOfDate.contains(getDate(2016, 2, 2)));
        assertTrue(listOfDate.contains(getDate(2016, 2, 3)));
        assertTrue(listOfDate.contains(getDate(2016, 2, 4)));
        assertTrue(listOfDate.contains(getDate(2016, 2, 5)));
        assertTrue(listOfDate.contains(getDate(2016, 2, 6)));
        assertTrue(listOfDate.contains(getDate(2016, 2, 7)));
        assertTrue(listOfDate.contains(getDate(2016, 2, 8)));
        assertTrue(listOfDate.contains(getDate(2016, 2, 9)));
        assertTrue(listOfDate.contains(getDate(2016, 2, 10)));

        listOfDate = DateUtils.getDatesBetween(date01, date01);
        assertNotNull(listOfDate);
        assertEquals(1, listOfDate.size());
        assertEquals(getDate(2016, 2, 1).getTime(), listOfDate.get(0).getTime());
    }

    @Test
    final void getDatesBetweenWithIllegalArgumentException()
    {
        assertThrows(IllegalArgumentException.class, () -> DateUtils.getDatesBetween(date10, date01));
    }

    @Test
    final void setTimeToBeginningOfDay()
    {}

    @Test
    final void setTimeToBeginningOfDayCalendar()
    {
        Calendar timeToEndOfDay = DateUtils.setTimeToBeginningOfDay(new Calendar.Builder().setInstant(date01).build());
        Calendar cal = new Calendar.Builder().setInstant(timeToEndOfDay.getTimeInMillis()).build();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        assertEquals(cal.getTimeInMillis(), timeToEndOfDay.getTimeInMillis());
    }

    @Test
    final void setTimeToBeginningOfDayDate()
    {
        Date timeToEndOfDay = DateUtils.setTimeToBeginningOfDay(date01);
        Calendar cal = new Calendar.Builder().setInstant(timeToEndOfDay).build();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        assertEquals(cal.getTimeInMillis(), timeToEndOfDay.getTime());
    }

    @Test
    final void setTimeToEndofDayCalendar()
    {
        Calendar timeToEndOfDay = DateUtils.setTimeToEndOfDay(new Calendar.Builder().setInstant(date01).build());
        Calendar cal = new Calendar.Builder().setInstant(timeToEndOfDay.getTimeInMillis()).build();
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);
        assertEquals(cal.getTimeInMillis(), timeToEndOfDay.getTimeInMillis());
    }

    @Test
    final void setTimeToEndOfDayDate()
    {
        Date timeToEndOfDay = DateUtils.setTimeToEndOfDay(date01);
        Calendar cal = new Calendar.Builder().setInstant(timeToEndOfDay).build();
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);
        assertEquals(cal.getTimeInMillis(), timeToEndOfDay.getTime());
    }

    @Test
    void setTimeToEndOfDay() throws Exception
    {
        Calendar calendarFromNow = DateUtils.getCalendarFromNow();
        assertNotNull(calendarFromNow);
    }

    @Test
    final void getDateBeginningMonth()
    {
        Date endDate = DateUtils.getDateBeginningMonth(date10);
        Calendar expectedCal = new Calendar.Builder().setInstant(date10).build();
        expectedCal.set(Calendar.DAY_OF_MONTH, 1);
        assertEquals(expectedCal.getTime().getTime(), endDate.getTime());
    }

    @Test
    final void getDateEndMonth()
    {
        Date endDate = DateUtils.getDateEndMonth(date10);
        Date expectedDate = new Calendar.Builder().setDate(2016, 2, 31).set(Calendar.HOUR_OF_DAY, 23).set(Calendar.MINUTE, 59)
                                                  .set(Calendar.SECOND, 59).set(Calendar.MILLISECOND, 999).build().getTime();
        assertEquals(expectedDate.getTime(), endDate.getTime());
    }

    @Test
    void average() throws Exception
    {}

    @Test
    final void averageWithDateRange()
    {
        DateRange dateRange = new DateRange(date01, date05);
        Date averageDate = DateUtils.average(dateRange);
        assertEquals(date03.getTime(), averageDate.getTime());
    }

    @Test
    final void averageWithListOfDate()
    {
        List<Date> dates = new LinkedList<>(ListUtils.createList(date01, date02, date03, date04, date05));
        Date averageDate = DateUtils.average(dates);
        assertEquals(date03.getTime(), averageDate.getTime());
    }

    @Test
    final void averageListOfDateWithIllegalArgumentException()
    {
        assertThrows(IllegalArgumentException.class, () -> DateUtils.average(Collections.emptyList()));
    }

    @Test
    final void untilDate()
    {
        List<Date> dates = new LinkedList<>(ListUtils.createList(date01, date02, date03, date04, date04, date05));
        Date dateReturned = DateUtils.untilDate(dates, date04);
        assertEquals(date04.getTime(), dateReturned.getTime());

        dates = new LinkedList<>(ListUtils.createList(date01, date02, date03, date04, date04, date05));
        dateReturned = DateUtils.untilDate(dates, date10);
        assertEquals(date05.getTime(), dateReturned.getTime());

        dates = new LinkedList<>(ListUtils.createList(date01, date02, date03, date05));
        dateReturned = DateUtils.untilDate(dates, date04);
        assertEquals(date03.getTime(), dateReturned.getTime());
    }

    @Test
    final void untilDateWithIllegalArgumentException()
    {
        assertThrows(IllegalArgumentException.class, () -> DateUtils.untilDate(Collections.emptyList(), date04));
    }
}