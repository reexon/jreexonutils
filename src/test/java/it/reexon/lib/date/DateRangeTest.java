package it.reexon.lib.date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.jupiter.api.Assertions.*;


/**
 * Created by Loris on 05/07/16.
 *
 */
class DateRangeTest
{

    private final static Date FROM = new GregorianCalendar(2020, 1, 1, 20, 0).getTime();
    private final static Date TO = new GregorianCalendar(2020, 1, 3, 22, 0).getTime();
    private DateRange dateRange;

    @BeforeEach
    void setUp() throws Exception
    {
        dateRange = new DateRange(FROM, TO);
    }

    @Test
    void DateRange() throws Exception
    {

        assertThrows(IllegalArgumentException.class, () -> new DateRange(TO, FROM));
    }

    @Test
    void setDateToLessThanFrom() throws Exception
    {
        assertThrows(IllegalArgumentException.class, () -> dateRange.setDateTo(new GregorianCalendar(2019, 1, 1, 10, 0).getTime()));
    }

    @Test
    void setDateFromGreaterThanTo() throws Exception
    {
        assertThrows(IllegalArgumentException.class, () -> dateRange.setDateFrom(new GregorianCalendar(2020, 1, 5, 10, 0).getTime()));
    }

    @Test
    void doesNotInclude() throws Exception
    {

        assertFalse(dateRange.include(new GregorianCalendar(2020, 1, 7, 15, 40).getTime()), "the date is included in range !");
    }

    @Test
    void getDateFrom() throws Exception
    {
        assertEquals(FROM, dateRange.getDateFrom());
    }

    @Test
    void setDateFrom() throws Exception
    {
        Date expected = new GregorianCalendar(2020, 1, 2, 23, 0).getTime();
        dateRange.setDateFrom(expected);
        assertEquals(expected, dateRange.getDateFrom());
    }

    @Test
    void getDateTo() throws Exception
    {
        assertEquals(TO, dateRange.getDateTo());
    }

    @Test
    void setDateTo() throws Exception
    {
        Date expected = new GregorianCalendar(2020, 1, 2, 10, 0).getTime();
        dateRange.setDateTo(expected);
        assertEquals(expected, dateRange.getDateTo());
    }

    @Test
    void getDuration() throws Exception
    {
        long expected = TO.getTime() - FROM.getTime();
        assertEquals(expected, dateRange.getDuration());
    }

    @Test
    void include() throws Exception
    {
        assertTrue(dateRange.include(new GregorianCalendar(2020, 1, 2, 15, 40).getTime()), "the date is not included in range");
    }

}