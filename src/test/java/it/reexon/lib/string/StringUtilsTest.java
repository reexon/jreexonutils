package it.reexon.lib.string;

import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;


/**
 * Created by Marco Velluto on 26/06/2017.
 *
 */
class StringUtilsTest
{
    @Test
    void toHexString()
    {
        byte[] bytes = { 0, 0 };
        StringUtils.toHexString(bytes);
    }

    @Test
    @Disabled
    void toHex()
    {
        @NotNull String sa = "ciao";
        StringUtils.toHex(sa);
    }

}