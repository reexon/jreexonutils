package it.reexon.lib.secure.algorithmics;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


/**
 * Created by Marco Velluto on 26/06/2017.
 *
 */
class MessageDigestAlgorithmsTest
{
    @Test
    void getDefault()
    {
        assertEquals("SHA-256", MessageDigestAlgorithms.getDefault());
    }

    @Test
    void values()
    {
        List<String> strings = Arrays.asList("MD2", "MD5", "SHA-1", "SHA-256", "SHA-384", "SHA-512");

        assertArrayEquals(strings.toArray(), MessageDigestAlgorithms.values().toArray());
    }

    @Test
    void testConstructorIsPrivate() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException
    {
        Constructor<MessageDigestAlgorithms> constructor = MessageDigestAlgorithms.class.getDeclaredConstructor();
        assertTrue(Modifier.isPrivate(constructor.getModifiers()));
        constructor.setAccessible(true);
        constructor.newInstance();
    }

}