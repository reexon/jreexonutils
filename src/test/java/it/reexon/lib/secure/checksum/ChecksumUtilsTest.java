/*
  Copyright (c) 2016 Marco Velluto. All rights reserved.
 */
package it.reexon.lib.secure.checksum;

import it.reexon.lib.file.FileUtils;
import it.reexon.lib.file.IOUtils;
import it.reexon.lib.list.ListUtils;
import it.reexon.lib.secure.algorithmics.MessageDigestAlgorithms;
import it.reexon.lib.string.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedList;
import java.util.List;

import static org.eclipse.persistence.jpa.jpql.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


/**
 * @author Marco Velluto
 */
@Disabled
class ChecksumUtilsTest
{
    private static final Logger logger = LogManager.getLogger(ChecksumUtilsTest.class);

    private static final String TOMCAT_CHECKSUM_MD5 = "857093659f35c3ee76de54cacc3a7e7e";

    private static final File DIRECTORY = new File("resources/tests/ChecksumUtilsTest_1");
    private static final File DIRECTORY_A = new File("resources/tests/ChecksumUtilsTest_1/DirecotyA");
    private static final File DIRECTORY_2 = new File("resources/tests/ChecksumUtilsTest_2");
    private static final File DIRECTORY_3 = new File("resources/tests/ChecksumUtilsTest_3");
    private static final File TOMCAT = new File("resources/tests/apache-tomcat-9.0.0.M3.zip");

    private static File fileA = null;
    private static File fileA1 = null;
    private static File fileB = null;
    private static File fileB1 = null;
    private static File fileB4 = null;
    private static File fileB5 = null;

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @BeforeAll
    static void setUpBeforeClass() throws Exception
    {
        if (!DIRECTORY.exists())
        {
            DIRECTORY.mkdirs();
        }

        if (!DIRECTORY_A.exists())
        {
            DIRECTORY_A.mkdir();
        }

        fileA = File.createTempFile("fileA", ".txt", DIRECTORY);
        List<String> linesA = new LinkedList<>(ListUtils.createList("CiaoA, Ciao1"));
        IOUtils.writeLines(fileA, linesA);
        fileA1 = File.createTempFile("fileA1", ".txt", DIRECTORY);
        IOUtils.writeLines(fileA1, linesA);

        fileB = File.createTempFile("fileB", ".txt", DIRECTORY);
        List<String> linesB = new LinkedList<>(ListUtils.createList("CiaoB, CiaoB"));
        IOUtils.writeLines(fileB, linesB);
        fileB1 = File.createTempFile("fileB1", ".txt", DIRECTORY);
        IOUtils.writeLines(fileB1, linesB);

        FileUtils.copyDirectory(DIRECTORY, DIRECTORY_2);

        if (!DIRECTORY_3.exists())
        {
            DIRECTORY_3.mkdirs();
        }

        fileB4 = File.createTempFile("fileB", ".txt", DIRECTORY_3);
        List<String> linesB3 = new LinkedList<>(ListUtils.createList("CiaoB3, CiaoB3"));
        IOUtils.writeLines(fileB4, linesB3);
        fileB5 = File.createTempFile("fileB3", ".txt", DIRECTORY_3);
        IOUtils.writeLines(fileB5, linesB);

    }

    @AfterAll
    static void tearDownAfterClass() throws Exception
    {
        FileUtils.forceDelete(DIRECTORY);
        FileUtils.forceDelete(DIRECTORY_A);
        FileUtils.forceDelete(DIRECTORY_2);
        FileUtils.forceDelete(DIRECTORY_3);
    }

    @Test
    @Disabled
    void testCreateChecksum() throws IOException, NoSuchAlgorithmException
    {
        byte[] checksumTomcat = ChecksumUtils.createChecksum(TOMCAT, MessageDigestAlgorithms.MD5);
        assertEquals(TOMCAT_CHECKSUM_MD5, StringUtils.toHexString(checksumTomcat));
        for (String algorithm : MessageDigestAlgorithms.values())
        {
            byte[] checksum1 = ChecksumUtils.createChecksum(fileA, algorithm);
            byte[] checksum2 = ChecksumUtils.createChecksum(fileA, algorithm);
            assertEquals(StringUtils.toHexString(checksum1), StringUtils.toHexString(checksum2));

            checksum1 = ChecksumUtils.createChecksum(fileA, algorithm);
            checksum2 = ChecksumUtils.createChecksum(fileA1, algorithm);
            assertEquals(StringUtils.toHexString(checksum1), StringUtils.toHexString(checksum2));

            checksum1 = ChecksumUtils.createChecksum(fileB, algorithm);
            checksum2 = ChecksumUtils.createChecksum(fileA1, algorithm);
            assertNotEquals(StringUtils.toHexString(checksum1), StringUtils.toHexString(checksum2));

            try
            {
                ChecksumUtils.createChecksum(new File(""), algorithm); //TODO SPOSTARE FUORI
                fail("Should have thrown an exception");
            }
            catch (Exception e)
            {
                assertEquals(FileNotFoundException.class, e.getClass());
            }
        }
    }

    @Test
    @Disabled
    void testGetChecksumFileMessageDigestAlgorithms() throws IOException, NoSuchAlgorithmException
    {
        String checksumTomcat = ChecksumUtils.getChecksum(TOMCAT, MessageDigestAlgorithms.MD5);
        assertEquals(TOMCAT_CHECKSUM_MD5, checksumTomcat);
        for (String algorithm : MessageDigestAlgorithms.values())
        {
            String checksum1 = ChecksumUtils.getChecksum(fileA, algorithm);
            String checksum2 = ChecksumUtils.getChecksum(fileA, algorithm);
            assertEquals(checksum1, checksum2);

            checksum1 = ChecksumUtils.getChecksum(fileA, algorithm);
            checksum2 = ChecksumUtils.getChecksum(fileA1, algorithm);
            assertEquals(checksum1, checksum2);

            checksum1 = ChecksumUtils.getChecksum(fileB, algorithm);
            checksum2 = ChecksumUtils.getChecksum(fileA1, algorithm);
            assertNotEquals(checksum1, checksum2);

            try //TODO SPOSTARE
            {
                ChecksumUtils.getChecksum(new File(""), algorithm);
                fail("Should have thrown an exception");
            }
            catch (Exception e)
            {
                assertEquals(FileNotFoundException.class, e.getClass());
            }
        }
    }

    @Test
    void testGetChecksumFile() throws IOException
    {
        String checksum1 = ChecksumUtils.getChecksum(fileA);
        String checksum2 = ChecksumUtils.getChecksum(fileA);
        assertEquals(checksum1, checksum2);

        checksum1 = ChecksumUtils.getChecksum(fileA);
        checksum2 = ChecksumUtils.getChecksum(fileA1);
        assertEquals(checksum1, checksum2);

        checksum1 = ChecksumUtils.getChecksum(fileB);
        checksum2 = ChecksumUtils.getChecksum(fileA1);
        assertNotEquals(checksum1, checksum2);

        try //TODO SPOSTARE
        {
            ChecksumUtils.getChecksum(new File(""));
            fail("Should have thrown an exception");
        }
        catch (Exception e)
        {
            assertEquals(FileNotFoundException.class, e.getClass());
        }
    }

    @Test
    void testWriteChecksumFromFile() throws IOException
    {
        ChecksumUtils.writeChecksumFromFile(DIRECTORY);
        List<File> files = IOUtils.importFiles(DIRECTORY, true);
        boolean isPresentSubDir = false;
        boolean isPresentRootDir = false;
        for (File file : files)
        {
            if (file.isDirectory())
            {
                File[] listFiles = file.listFiles();
                if (listFiles != null)
                {
                    for (File subFile : listFiles)
                        if (subFile.getName().contains("checksum"))
                        {
                            isPresentSubDir = true;
                        }
                }
            }
            else
            {
                if (file.getName().contains("checksum"))
                {
                    isPresentRootDir = true;
                }
            }
        }
        assertEquals(true, isPresentSubDir);
        assertEquals(true, isPresentRootDir);

        try
        {
            ChecksumUtils.writeChecksumFromFile(new File("")); //TODO SPOSTARE
            fail("Should have thrown an exception");
        }
        catch (Exception e)
        {
            assertEquals(FileNotFoundException.class, e.getClass());
        }
    }
}
