/*
  Copyright (c) 2016 Marco Velluto. All rights reserved.
 */
package it.reexon.lib.secure.serial.number;

import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import org.junit.jupiter.api.Test;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;


/**
 * @author marco.velluto
 * @since Java 1.8
 */
class SerialNumberTest
{

    @SuppressWarnings("unused")
    private static String generateProductKey() throws NoSuchAlgorithmException
    {
        Long code = SecureRandom.getInstance("SHA1PRNG").nextLong();
        final HashFunction hashFunction = Hashing.sha512();
        final HashCode hashCode = hashFunction.hashString(code.toString());
        return hashCode.toString().toUpperCase();
    }

    @Test
    void testCreateLicenseKey() throws Exception
    {
        String userName = "Max Headroom";
        String productKey = "ABCD";
        String versionNumber = "4";
        String key = SerialNumber.createLicenseKey(userName, productKey, versionNumber);
        System.out.println("Key: " + key);

        userName = "marco.velluto@gmail.com";
        productKey = "588BF51AE610C2532965B027D2F60BF422E4508A33413FEE8C9C9E3D9B05AF2664090377F7081717209AF451A63E6203976D95B439EF1EEFF131DE13273E351C"; //generateProductKey();
        versionNumber = "4.1.22";
        key = SerialNumber.createLicenseKey(userName, productKey, versionNumber);
        System.out.println("Key: " + key);
    }

}
