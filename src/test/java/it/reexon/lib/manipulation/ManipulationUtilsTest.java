package it.reexon.lib.manipulation;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertTrue;


/**
 * Created by Marco Velluto on 26/06/2017.
 *
 */
class ManipulationUtilsTest
{
    @Test
    void testConstructorIsPrivate() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException
    {
        Constructor<ManipulationUtils> constructor = ManipulationUtils.class.getDeclaredConstructor();
        assertTrue(Modifier.isPrivate(constructor.getModifiers()));
        constructor.setAccessible(true);
        constructor.newInstance();
    }

    @Test
    void generatePermutations()
    {
        ManipulationUtils.generatePermutations(Collections.emptyList());
    }

    @Test
    void permutatiosCount()
    {
        ManipulationUtils.permutatiosCount(0);
    }

    @Test
    void shuffle()
    {
        ManipulationUtils.shuffle("CIAO");
    }

    @Test
    void permutatios()
    {
        ManipulationUtils.permutatios("CIAO");
    }

}