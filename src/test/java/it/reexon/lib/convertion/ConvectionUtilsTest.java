package it.reexon.lib.convertion;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


/**
 * Created by Marco Velluto on 12/06/2017.
 *
 */
class ConvectionUtilsTest
{
    @Test
    void convertByteInMB()
    {
        assertEquals(244.140625, ConvectionUtils.convertByteInMB(256000000));
        assertEquals(244.140625, ConvectionUtils.convertByteInMB(256000000D));
        assertEquals(244.140625, ConvectionUtils.convertByteInMB(256000000L));
        assertEquals(244.140625, ConvectionUtils.convertByteInMB(new BigDecimal("256000000")));
    }

    @Test
    void testConstructorIsPrivate() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException
    {
        Constructor<ConvectionUtils> constructor = ConvectionUtils.class.getDeclaredConstructor();
        assertTrue(Modifier.isPrivate(constructor.getModifiers()));
        constructor.setAccessible(true);
        constructor.newInstance();
    }
}