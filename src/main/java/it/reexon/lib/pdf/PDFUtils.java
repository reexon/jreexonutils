/*
  Copyright (c) 2016 Marco Velluto
 */
package it.reexon.lib.pdf;

import it.reexon.lib.list.ListUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.exceptions.CryptographyException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.pdmodel.encryption.BadSecurityHandlerException;
import org.apache.pdfbox.pdmodel.encryption.StandardProtectionPolicy;
import org.jetbrains.annotations.NotNull;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;


/**
 * @author Marco Velluto
 */
//TODO Aggiornare a PDFBox 2.0
public class PDFUtils
{
    private static final Logger logger = LogManager.getLogger(PDFUtils.class);

    /** Owner password. */
    private static final String OWNER = "UzsPJoVh7hjzv2kdL3Vf";

    /**
     * Extracts and saves the file system each PDF page as if it were an extension PNG
     *
     * @param sourcePDF pdf to fetch images
     * @param destinationFolder destination folder
     *
     * @throws IOException              If there is an error reading from the stream.
     * @throws FileNotFoundException    If sourcePDF doesen't exist
     * @throws IllegalArgumentException If sourcePDF or destinationFolder are null.
     */
    public static void extractImages(@NotNull File sourcePDF, @NotNull File destinationFolder) throws IOException
    {
        if (!destinationFolder.exists())
        {
            destinationFolder.mkdir();
            logger.trace("Folder Created -> " + destinationFolder.getAbsolutePath());
        }
        if (sourcePDF.exists())
        {
            try (PDDocument document = PDDocument.load(sourcePDF))
            {
                logger.trace("Images copied to Folder: " + destinationFolder.getName());

                List<?> list = document.getDocumentCatalog().getAllPages();
                List<PDPage> listPage = ListUtils.castList(PDPage.class, list);

                logger.trace("Total files to be converted -> " + list.size());

                String fileName = sourcePDF.getName().replace(".pdf", "");
                int pageNumber = 1;
                for (PDPage page : listPage)
                {
                    BufferedImage image = page.convertToImage();
                    File outputfile = new File(destinationFolder + "/" + fileName + "_" + pageNumber + ".png");
                    logger.trace("Image Created -> " + outputfile.getName());
                    ImageIO.write(image, "png", outputfile);
                    pageNumber++;
                }
                logger.trace("Converted Images are saved at -> " + destinationFolder.getAbsolutePath());
            }
        }
        else
        {
            throw new FileNotFoundException(sourcePDF.getName() + " File not exists");
        }
    }

    /**
     * Encrypt a PDF with A
     *
     * @param src       src pdf to crypt
     * @param dest      destination to pdf crypted
     * @throws IOException              An I/O error
     * @throws CryptographyException    If an error occurs during encryption.
     * @throws COSVisitorException      If an error occurs while generating the data.           
     * @throws IllegalArgumentException If some of the parameters is empty or null.
     */
    public static void encryptPdf(@NotNull String src, @NotNull String dest, @NotNull String userPassword)
            throws IOException, CryptographyException, COSVisitorException
    {
        try (PDDocument doc = PDDocument.load(src))
        {
            doc.encrypt(OWNER, userPassword);
            doc.save(dest);
        }
    }

    /**
     * Decrypt a PDF
     * <pre>
     *  NOTE: works only:
     * Acrobat 3 (40-bit RC4)      -> works
     * Acrobat 5 & 6 (128-bit RC4) -> works
     * Acrobat 7 (128-bit AES)     -> works
     * Acrobat 9 (256-bit AES)     -> doesn't work
     * </pre>
     *
     * @param src       src pdf to crypt
     * @param dest      destination to pdf crypted
     * @throws IOException              An I/O error
     * @throws CryptographyException    If there is an error decrypting the document.
     * @throws COSVisitorException      If an error occurs while generating the data.
     * @throws IllegalArgumentException If some of the parameters is empty or null
     */
    public static void decrypt(@NotNull String src, @NotNull String dest, @NotNull String userPassword)
            throws IOException, CryptographyException, COSVisitorException
    {
        try (PDDocument doc = PDDocument.load(src))
        {
            if (doc.isEncrypted())
            {
                doc.decrypt(userPassword);
                doc.setAllSecurityToBeRemoved(true);
            }
            doc.save(dest);
        }
    }

    /**
     * Modify PDF permissions and only allows you to print the document.
     *
     * @param srcFile           Begin file.
     * @param destFile          Destination file.  
     * @param password          User password. Can be null.
     *
     * @throws IllegalArgumentException         If begin file or destination file are null.
     * @throws IOException                      If there is an error saving the document.
     * @throws BadSecurityHandlerException      If there is an error during protection.
     * @throws COSVisitorException              If an error occurs while generating the data.
     */
    public static void setAccessPermissionOnlyPrint(@NotNull File srcFile, @NotNull File destFile, @NotNull String password)
            throws IOException, BadSecurityHandlerException, COSVisitorException
    {
        try (PDDocument doc = PDDocument.load(srcFile))
        {
            AccessPermission perms = new AccessPermission(3);
            perms.setCanPrint(true);
            StandardProtectionPolicy sp = new StandardProtectionPolicy(OWNER, password, perms);
            doc.protect(sp);
            doc.save(destFile);
        }
    }

    /**
     * Return true if PDF document is encrypted
     *
     * @param src                       src pdf to crypt
     * @return true if PDF docuemnt is encrypted
     * @throws IOException              If there is an error reading from the stream.
     * @throws IllegalArgumentException If src is null or blank
     */
    public static boolean isEncrypted(@NotNull String src) throws IOException
    {
        try (PDDocument doc = PDDocument.load(src))
        {
            return doc.isEncrypted();
        }
    }

}
