/*
  Copyright (c) 2016 Marco Velluto
 */
package it.reexon.lib.date;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Date;


/**
 * @author Marco VellutoS
 */
@SuppressWarnings("WeakerAccess")
public class DateRange
{
    private Date dateFrom;
    private Date dateTo;

    public DateRange(@NotNull Date dateFrom, @NotNull Date dateTo)
    {
        super();

        checkDate(dateFrom, dateTo);

        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
    }

    /**
     * Check if dates are with sense
     */
    @Contract("null, _ -> fail; !null, null -> fail")
    private static void checkDate(@NotNull Date from, @NotNull Date to)
    {
        if (from.getTime() > to.getTime())
        {
            throw new IllegalArgumentException("Starting date should be less than ending one");
        }
    }

    public Date getDateFrom()
    {
        return dateFrom;
    }

    public void setDateFrom(@NotNull Date dateFrom)
    {
        checkDate(dateFrom, this.dateTo);
        this.dateFrom = dateFrom;
    }

    public Date getDateTo()
    {
        return dateTo;
    }

    public void setDateTo(@NotNull Date dateTo)
    {
        checkDate(this.dateFrom, dateTo);
        this.dateTo = dateTo;
    }

    /**
     *
     * @param date date to check
     * @return true if given date is between the range
     */
    public boolean include(@NotNull Date date)
    {
        return DateUtils.between(date, dateFrom, dateTo);
    }

    /**
     *
     * @return duration between 2 dates in ms
     */
    public long getDuration()
    {
        return dateTo.getTime() - dateFrom.getTime();
    }
}
