/*
 *  Copyright (c) 2016 Marco Velluto
 */
package it.reexon.lib.date;

import it.reexon.lib.list.ListUtils;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.stream.Collectors;


/**
 * @author Marco Velluto
 * @since 1.0
 */
@SuppressWarnings("WeakerAccess")
public class DateUtils
{
    private DateUtils()
    {/* Could not instance it*/}

    /**
     * Add days to the date
     *
     * @param date Date on which to add the days
     * @param numberDays number of days
     * @return date - Date on which you have added days 
     *
     * @since 1.0
     *
     * @throws IllegalArgumentException if date is null
     */
    @NotNull
    @Contract("null, _ -> fail")
    public static Date addDays(@NotNull final Date date, int numberDays)
    {
        final Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, numberDays);

        return c.getTime();
    }

    /**
     * Add one day to the date
     *
     * @param date Date on which to add one day
     * @return date - Date on which you have added one day
     *
     * @since 1.0
     *
     * @throws IllegalArgumentException if date is null
     */
    @NotNull
    @Contract("null -> fail")
    public static Date addOneDay(@NotNull final Date date)
    {
        return addDays(date, 1);
    }

    /**
     * Checks whether the date is between the start date and end date
     *
     * @param dateToCheck date to check
     * @param startDate start date
     * @param endDate end date
     * @return true - if the date is within the range provided
     *
     * @since 1.0
     *
     * @throws IllegalArgumentException if dateToCheck, startDate, endDate are null.
     */
    @Contract("null, _, _ -> fail; !null, null, _ -> fail; !null, !null, null -> fail")
    public static boolean between(@NotNull final Date dateToCheck, @NotNull final Date startDate, @NotNull final Date endDate)
    {
        if (startDate.getTime() > endDate.getTime())
        {
            throw new IllegalArgumentException("endDate must be max startDate");
        }

        return dateToCheck.compareTo(startDate) >= 0 && dateToCheck.compareTo(endDate) <= 0;
    }

    /**
     * Generate Calendar from new
     *
     * @return calendar - calendar from now
     *
     * @since 1.0
     */
    public static Calendar getCalendarFromNow()
    {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());

        return calendar;
    }

    /**
     * Generates an list of data from first to last
     *
     * @param date1 min date
     * @param date2 max date
     * @return list of dates 
     *
     * @Example
     *  param date1 = 2016/03/01
     *  param date2 = 2016/03/31
     *
     *  return Set{2016/03/01, 2016/03/02, ... , 2016/03/31}
     *
     *  @since 1.0
     *
     *  @throws IllegalArgumentException if date2 > date1
     */
    @Contract("null, _ -> fail; !null, null -> fail")
    public static List<Date> getDatesBetween(@NotNull final Date date1, @NotNull final Date date2)
    {
        if (date2.getTime() < date1.getTime())
        {
            throw new IllegalArgumentException("Date2 have to max date1");
        }

        List<Date> dates = new LinkedList<>();
        final Calendar calendarDate1 = Calendar.getInstance();
        calendarDate1.setTimeInMillis(date1.getTime());

        final Calendar calendarDate2 = Calendar.getInstance();
        calendarDate2.setTimeInMillis(date2.getTime());

        Long diffDay = (calendarDate2.getTimeInMillis() - calendarDate1.getTimeInMillis()) / (1000 * 60 * 60 * 24);

        dates.add(new Date(calendarDate1.getTimeInMillis()));
        for (int c = 0; c < diffDay; c++)
        {
            calendarDate1.add(Calendar.DATE, 1);
            dates.add(new Date(calendarDate1.getTimeInMillis()));
        }
        return dates;
    }

    /**
     * Set Time To Beginning Of Day
     *
     * The Calendar sets with the following parameters of the day at 0 :
     * <br>* HOUR_OF_DAY
     * <br>* MINUTE
     * <br>* SECOND
     * <br>* MILLISECOND
     * @param calendar calendar to be set
     * @return date - date set as the beginning of the day
     *
     * @since 1.0
     *
     * @throws IllegalArgumentException if calendar is null
     */
    @Contract("null -> fail")
    public static Calendar setTimeToBeginningOfDay(@NotNull final Calendar calendar)
    {
        Calendar c = (Calendar) calendar.clone();

        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return c;
    }

    /**
     * Set Time To Beginning Of Day
     *
     * The Date sets with the following parameters of the day at 0 :
     * * HOUR_OF_DAY
     * * MINUTE
     * * SECOND
     * * MILLISECOND
     *
     * @param date date to be set
     * @return date - Date set as the beginning of the day
     *
     * @since 1.0
     *
     * @throws IllegalArgumentException if date is null
     */
    @NotNull
    @Contract("null -> fail")
    public static Date setTimeToBeginningOfDay(@NotNull final Date date)
    {
        final Calendar startCalendar = Calendar.getInstance();
        startCalendar.setTime(date);
        startCalendar.set(Calendar.HOUR_OF_DAY, 0);
        startCalendar.set(Calendar.MINUTE, 0);
        startCalendar.set(Calendar.SECOND, 0);
        startCalendar.set(Calendar.MILLISECOND, 0);

        return startCalendar.getTime();
    }

    /**
     * Set Time To End of Day
     *
     * The Calendar sets with the following parameters of the day at:
     * * HOUR_OF_DAY -> 23
     * * MINUTE -> 59
     * * SECOND -> 59
     * * MILLISECOND -> 999
     *
     * @param calendar calendar to be set
     * @return caledar - calendar set as the ending of the day
     *
     * @since 1.0
     *
     * @throws IllegalArgumentException if calendar is null
     */
    @Contract("null -> fail")
    public static Calendar setTimeToEndOfDay(@NotNull final Calendar calendar)
    {
        final Calendar c = (Calendar) calendar.clone();

        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        c.set(Calendar.MILLISECOND, 999);

        return c;
    }

    /**
     * Set Time To End of Day
     *
     * The Calendar sets with the following parameters of the day at:
     * * HOUR_OF_DAY -> 23
     * * MINUTE -> 59
     * * SECOND -> 59
     * * MILLISECOND -> 999
     *
     * @param date date to be set
     * @return date - date set as the ending of the day
     *
     * @since 1.0
     *
     * @throws IllegalArgumentException if date is null
     */
    @NotNull
    public static Date setTimeToEndOfDay(@NotNull final Date date)
    {
        Calendar calendarEnd = Calendar.getInstance();
        calendarEnd.setTime(date);
        calendarEnd.set(Calendar.HOUR_OF_DAY, 23);
        calendarEnd.set(Calendar.MINUTE, 59);
        calendarEnd.set(Calendar.SECOND, 59);
        calendarEnd.set(Calendar.MILLISECOND, 999);
        return calendarEnd.getTime();
    }

    /**
     * Returns the date with the beginning of the month
     *
     * @param date date to get beginning month
     * @return data - date of beginning month 
     *
     * @since 1.0
     *
     * @throws IllegalArgumentException if date is null
     */
    @NotNull
    public static Date getDateBeginningMonth(@NotNull final Date date)
    {
        Calendar calendarBegin = Calendar.getInstance();
        calendarBegin.setTime(date);
        calendarBegin.set(Calendar.DAY_OF_MONTH, calendarBegin.getActualMinimum(Calendar.DAY_OF_MONTH));

        return setTimeToBeginningOfDay(calendarBegin).getTime();

    }

    /**
     * Returns the date with the ending of the month
     *
     * @param date date to get ending month
     * @return data - date of ending month
     *
     * @since 1.0
     *
     * @throws IllegalArgumentException if date is null 
     */
    @NotNull
    @Contract("null -> fail")
    public static Date getDateEndMonth(@NotNull final Date date)
    {
        final Calendar calendarEnd = Calendar.getInstance();
        calendarEnd.setTime(date);
        calendarEnd.set(Calendar.DAY_OF_MONTH, calendarEnd.getActualMaximum(Calendar.DAY_OF_MONTH));

        return setTimeToEndOfDay(calendarEnd).getTime();
    }

    /**
     * Calculates the average of the proposed dates
     *
     * @param dateRange dates by which to calculate the average
     * @return - average date
     *
     * @since 1.0
     *
     * @throws NullPointerException if date is null
     * @throws NoSuchElementException if all the dates in the date range are null
     * @throws IllegalArgumentException if dates in date range is null
     */
    @NotNull
    @Contract("null -> fail")
    public static Date average(@NotNull DateRange dateRange)
    {
        List<Date> dateList = new LinkedList<>();
        dateList.add(dateRange.getDateFrom());
        dateList.add(dateRange.getDateTo());
        return average(dateList);
    }

    /**
     * Calculates the average of the proposed dates
     *
     * @param dates dates by which to calculate the average
     * @return average date
     *
     * @since 1.0
     *
     * @throws IllegalArgumentException if all the dates in the date range are null
     */
    @NotNull
    @Contract("null -> fail")
    public static Date average(@NotNull List<Date> dates)
    {
        if (dates.isEmpty() || dates.stream().filter(Objects::nonNull).count() < 1)
        {
            throw new IllegalArgumentException("No element found in dates");
        }

        OptionalDouble millisecond = dates.stream().mapToLong(Date::getTime).average();

        return new Date((long) (millisecond.isPresent() ? millisecond.getAsDouble() : 0L));
    }

    /**
     * It takes the date that is closest to the one passed as a parameter.
     *
     * @param dates date list from which take the value
     * @param date  date to looking for
     *
     * @return date that is closest to that past.
     *
     * @since 1.0
     *
     * @throws IllegalArgumentException if dates is empty
     *
     */
    @NotNull
    public static Date untilDate(@NotNull List<Date> dates, @NotNull Date date)
    {
        if (dates.isEmpty())
        {
            throw new IllegalArgumentException("List of date cannot be empty");
        }

        List<Long> datesLong = new ArrayList<>(dates.stream().map(Date::getTime).collect(Collectors.toList()));
        Long dateLong = ListUtils.nearestElement(datesLong, date.getTime());

        return new Date(dateLong);
    }
}
