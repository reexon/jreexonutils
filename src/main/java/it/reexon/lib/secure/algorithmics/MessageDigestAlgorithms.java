package it.reexon.lib.secure.algorithmics;

import it.reexon.lib.list.ListUtils;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.List;


/**
 * The algorithm names in this section can be specified when generating an instance of MessageDigest.
 *
 * @author marco.velluto
 * @link http://docs.oracle.com/javase/8/docs/technotes/guides/security/StandardNames.html#MessageDigest
 */
public class MessageDigestAlgorithms
{
    /**
     * The MD5 message digest algorithm defined in RFC 1321.
     */
    public static final String MD5 = "MD5";
    /**
     * The SHA-256 hash algorithm defined in the FIPS PUB 180-2.
     */
    public static final String SHA_256 = "SHA-256";
    /**
     * The MD2 message digest algorithm as defined in RFC 1319.
     */
    private static final String MD2 = "MD2";
    /**
     * The SHA-1 hash algorithm defined in the FIPS PUB 180-2.
     */
    private static final String SHA_1 = "SHA-1";
    /**
     * The SHA-384 hash algorithm defined in the FIPS PUB 180-2.
     */
    private static final String SHA_384 = "SHA-384";
    /**
     * The SHA-512 hash algorithm defined in the FIPS PUB 180-2.
     */
    private static final String SHA_512 = "SHA-512";

    private MessageDigestAlgorithms()
    {
        // cannot be instantiated.
    }

    /**
     * @return SHA_256
     */
    @Contract(pure = true)
    @SuppressWarnings("SameReturnValue")
    public static String getDefault()
    {
        return SHA_256;
    }

    /**
     * @return Every algorithms value
     */
    @NotNull
    public static List<String> values()
    {
        return ListUtils.createList(MD2, MD5, SHA_1, SHA_256, SHA_384, SHA_512);
    }

}
