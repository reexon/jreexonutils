package it.reexon.lib.secure.serial.number;

import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import org.jetbrains.annotations.NotNull;


/**
 * Questa classe permette di generare Serial Number da usare come licenza
 * per i propri software
 *
 * @author Marco Velluto
 * @since 1.0
 */
@SuppressWarnings("WeakerAccess")
public class SerialNumber
{
    @NotNull
    public static String createLicenseKey(@NotNull String userName, @NotNull String productKey, @NotNull String versionNumber)
    {
        final String s = userName + "|" + productKey + "|" + versionNumber;
        final HashFunction hashFunction = Hashing.sha1();
        final HashCode hashCode = hashFunction.hashString(s);
        final String upper = hashCode.toString().toUpperCase();
        return group(upper);
    }

    @NotNull
    private static String group(@NotNull String s)
    {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < s.length(); i++)
        {
            if (i % 6 == 0 && i > 0)
            {
                result.append('-');
            }
            result.append(s.charAt(i));
        }
        return result.toString();
    }
}
