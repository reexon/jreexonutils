/*
  Copyright (c) 2016 Marco Velluto. All rights reserved.
 */
package it.reexon.lib.secure.checksum;

import it.reexon.lib.file.IOUtils;
import it.reexon.lib.secure.algorithmics.MessageDigestAlgorithms;
import it.reexon.lib.string.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedList;
import java.util.List;


/**
 * @author Marco Velluto
 */
@SuppressWarnings("WeakerAccess")
public class ChecksumUtils
{
    private static final byte[] BUFFER = new byte[1024];
    private static final String PATTERN = " - Checksum " + MessageDigestAlgorithms.getDefault() + ": ";
    private static final Logger logger = LogManager.getLogger(ChecksumUtils.class);

    /**
     * Create a byte[] checksum from file name
     *
     * @param file  file to generate checksum
     * @param algorithm the name of the algorithm requested.
     * See the MessageDigest section in the <a href=
     * Java Cryptography Architecture Standard Algorithm Name Documentation</a>
     * for information about standard algorithm names.
     * @return byte[] checksum
     *
     * @throws IOException              If some other I/O error occurs.
     * @throws NoSuchAlgorithmException If no Provider supports a MessageDigestSpi implementation for the specified algorithm.
     */
    public static byte[] createChecksum(@NotNull File file, @NotNull String algorithm) throws IOException, NoSuchAlgorithmException
    {
        if (!file.exists())
        {
            throw new FileNotFoundException("File not found!");
        }

        try (InputStream fis = new FileInputStream(file))
        {
            MessageDigest complete = MessageDigest.getInstance(algorithm);
            int numRead;

            do
            {
                numRead = fis.read(BUFFER);
                if (numRead > 0)
                {
                    complete.update(BUFFER, 0, numRead);
                }
            }
            while (numRead != -1);
            return complete.digest();
        }
    }

    /**
     * Generate a String checksum
     *
     * @param file      file to generate checksum
     * @param algorithm Algorithm with which to generate the string
     *
     * @return String checksum
     *
     * @throws IOException              If some other I/O error occurs.
     * @throws NoSuchAlgorithmException If no Provider supports a MessageDigestSpi implementation for the specified algorithm.
     */
    @NotNull
    public static String getChecksum(@NotNull File file, @NotNull String algorithm) throws IOException, NoSuchAlgorithmException
    {
        byte[] checksum = createChecksum(file, algorithm);
        return StringUtils.toHexString(checksum);
    }

    /**
     * Generate a String checksum with MessageDigestAlgorithms SHA-256
     *
     * @param file  filename to generate checksum
     *
     * @return String checksum
     *
     * @throws IOException              If some other I/O error occurs.
     * @throws IllegalArgumentException If file is null.  
     */
    @NotNull
    public static String getChecksum(@NotNull File file) throws IOException
    {
        try
        {
            byte[] checksum;
            checksum = createChecksum(file, MessageDigestAlgorithms.getDefault());
            return StringUtils.toHexString(checksum);
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new RuntimeException(e);
        }
    }

    // TODO public static void writeChecksumFromFile(File file, boolean continueWithErrors);

    /**
     * Generates a file containing all checksum of the files in those folders .
     * Create a file in any of the specified below directory.
     *
     * @param file Files to generate the checksum
     *
     * @throws IOException              If some other I/O error occurs.
     * @throws IllegalArgumentException If file is null
     */
    public static void writeChecksumFromFile(@NotNull File file) throws IOException
    {
        if (!file.exists())
        {
            throw new FileNotFoundException("File must be exists");
        }

        if (file.isDirectory())
        {
            String path = file.getPath();
            File txtFile = new File(path + "/checksum.txt");
            List<File> files = IOUtils.importFiles(file, true);
            List<String> checksumList = new LinkedList<>();
            int c = 1;
            for (File fileToCheck : files)
            {
                if (fileToCheck.isDirectory())
                {
                    writeChecksumFromFile(fileToCheck);
                }
                else
                {
                    try
                    {
                        checksumList.add(c + ". " + fileToCheck.getName() + PATTERN + getChecksum(fileToCheck));
                        c++;
                    }
                    catch (IOException e)
                    {
                        logger.error(e.getMessage(), e);
                    }
                }
            }
            try
            {
                org.apache.commons.io.FileUtils.writeLines(txtFile, checksumList);
            }
            catch (IOException e)
            {
                logger.error(e.getMessage(), e);
            }
        }
        if (file.isFile())
        {
            String path = file.getPath();
            File txtFile = new File(path + "\\checksum-" + file.getName() + ".txt");
            List<String> checksum = new LinkedList<>();
            checksum.add(file.getName() + PATTERN + getChecksum(file));
            try
            {
                org.apache.commons.io.FileUtils.writeLines(txtFile, checksum);
            }
            catch (IOException e)
            {
                logger.error(e.getMessage(), e);
            }
        }
    }
}
