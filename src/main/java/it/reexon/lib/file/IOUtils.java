/*
  Copyright (c) 2016 Marco Velluto
 */
package it.reexon.lib.file;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;


/**
 * @author Marco Velluto
 * @since 1.0
 */
@SuppressWarnings("WeakerAccess")
public class IOUtils
{
    /**
     * @deprecated use
     * Generates a list of files. Contains only file. Non contains sub-directory
     *
     * @param folderFile folder from which to import files 
     * @return list of files
     *
     * @since 1.0
     *
     * @throws IllegalArgumentException If the folderFile is not a folder
     * @throws NullPointerException     If folderFile is null
     */
    @Deprecated
    public static List<File> importFiles(@NotNull File folderFile) throws IllegalArgumentException, NullPointerException
    {
        if (!folderFile.isDirectory())
        {
            throw new IllegalArgumentException("FolderPath param is not a folder!! ");
        }
        List<File> files = new LinkedList<>();

        File[] listOfFile = folderFile.listFiles();
        if (listOfFile == null)
            return Collections.emptyList();

        for (File aListOfFile : listOfFile)
        {
            if (aListOfFile.isFile())
            {
                files.add(aListOfFile);
            }
        }
        return files;
    }

    /**
     * Generates a list of files
     *
     * @param folderFile folder from which to import files
     * @param subDirectories true if contains sub directory
     * @return list of files
     *
     * @since 1.0
     *
     * @throws IllegalArgumentException If the folderFile is not a folder
     * @throws NullPointerException     If folderFile is null
     */
    public static List<File> importFiles(File folderFile, boolean subDirectories) throws IllegalArgumentException, NullPointerException
    {
        if (folderFile == null)
        {
            throw new NullPointerException("folderFile is null");
        }
        if (!folderFile.isDirectory())
        {
            throw new IllegalArgumentException("FolderPath param is not a folder!! ");
        }
        Set<File> files = new HashSet<>();

        File[] listOfFile = folderFile.listFiles();
        for (File aListOfFile : listOfFile != null ? listOfFile : new File[0])
        {
            if (subDirectories)
            {
                files.add(aListOfFile);
            }

            else if (aListOfFile.isFile())
            {
                files.add(aListOfFile);
            }
        }
        return new ArrayList<>(files);
    }

    //TODO      public static List<File> importOnlyDirector(File folderFile, boolean subDirectories) throws IllegalArgumentException, NullPointerException

    /**
     * Move file in a new directory     
     *
     * @param file to move
     * @param targgetDirectory target directory
     *
     * @since 1.0
     *
     * @throws IOException If some other I/O error occurs.
     */
    public static void moveFile(File file, String targgetDirectory) throws IOException
    {
        if (!new File(targgetDirectory).exists())
        {
            boolean mkdirs = new File(targgetDirectory).mkdirs();
            if (!mkdirs)
                throw new IOException("Cannot create directory " + targgetDirectory);
        }
        if (file.renameTo(new File(targgetDirectory + "\\" + file.getName())))
        {
            System.out.println("File is moved successful!");
        }
        else
        {
            throw new IOException("The called file " + file.getName() + " is not present into " + targgetDirectory);
        }
    }

    /**
     * Write lines on selected file
     *
     * @param file file to write
     * @param lines to be write
     *
     * @since 1.0
     *
     * @throws IOException If some other I/O error occurs.
     */
    public static void writeLines(File file, List<String> lines) throws IOException
    {
        try (FileOutputStream fileOutputStream = new FileOutputStream(file))
        {
            org.apache.commons.io.IOUtils.writeLines(lines, null, fileOutputStream, "UTF-8");
        }
    }
}
