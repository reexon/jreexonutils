package it.reexon.lib.file.zip;

/*
  Copyright (c) 2016 Marco Velluto

  Licensed to the Apache Software Foundation (ASF) under one or more
  contributor license agreements.  See the NOTICE file distributed with
  this work for additional information regarding copyright ownership.
  The ASF licenses this file to You under the Apache License, Version 2.0
  (the "License"); you may not use this file except in compliance with
  the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;


/**
 * @author Marco Velluto
 * @since 1.0
 *
 */
@SuppressWarnings("WeakerAccess")
public class ZipUtils
{
    private static final int BUFSIZE = 1024;

    private ZipUtils()
    {/* Could not instance it*/}

    //TODO:  public static void createZipOfDirectory(File sourceDir, File zipOutput, int level) throws IOException, IllegalArgumentException

    //TODO Add method to create zip to crypt

    /**
     * Create Zip of File Directory
     *
     * @param sourceDir source directory, this one will not be included on zip archive.
     *                  All content of sourceDir will be archived as "root" of zip archive.
     *                  Ex:
     *
     *                  example/
     *                      some_file.txt
     *                      some_video.mp4
     *                      another_dir/
     *
     *                  createZipFileOfDirectory("example" ,...)
     *
     *                  the results zip will be:
     *                  ./
     *                      some_file.txt
     *                      some_video.mp4
     *                      another_dir/
     *
     * @param zipOutput zip output file
     *
     * @throws IOException - If the file exists but is a directory rather than a regular file, does not exist but cannot be created, or cannot be opened for any other reason
     */
    public static void createZipFileOfDirectory(@NotNull File sourceDir, @NotNull File zipOutput) throws IOException
    {
        if (!sourceDir.exists())
        {
            throw new IllegalArgumentException(sourceDir.getAbsolutePath() + " is not exists!");
        }

        String baseName = sourceDir.getAbsolutePath() + File.pathSeparator;

        try (ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(zipOutput)))
        {
            addDirToZip(sourceDir, zipOutputStream, zipOutput, baseName);
        }
    }

    /**
     * Create zip from file list
     *
     * @param files file to zip
     * @param out stream out file
     *
     * @throws IOException if an I/O error has occurred
     */
    public static void createZip(@NotNull List<File> files, @NotNull OutputStream out) throws IOException
    {

        try (ZipOutputStream zout = new ZipOutputStream(out))
        {
            for (File file : files)
            {
                createZip(file, zout);
            }
        }
    }

    /**
     * Create zip from file
     *
     * @param file file to zip
     * @param out OutputStream
     *
     * @throws IOException  if an I/O error has occurred
     */
    public static void createZip(@NotNull File file, @NotNull OutputStream out) throws IOException
    {
        try (ZipOutputStream zout = new ZipOutputStream(out))
        {
            createZip(file, zout);
        }
    }

    /**
     * Create un zip file
     *
     * @param file file to zip
     * @param zout ZipOutputStream
     *
     * @throws IOException if an I/O error has occurred
     */
    public static void createZip(@NotNull File file, @NotNull ZipOutputStream zout) throws IOException
    {
        String name = file.getName();

        ZipEntry entry = new ZipEntry(name);
        entry.setTime(file.lastModified());
        zout.putNextEntry(entry);

        byte[] buf = new byte[BUFSIZE];

        try (FileInputStream is = new FileInputStream(file))
        {
            int l;
            while ((l = is.read(buf)) > -1)
            {
                zout.write(buf, 0, l);
            }
        }
    }

    /**
     * Unzip file
     *
     * @param is inputStream file to zip
     * @param targetDir destination directory
     *
     * @throws IOException if an I/O error has occurred
     */
    public static void unzip(@NotNull InputStream is, @NotNull File targetDir) throws IOException
    {
        ZipEntry entry;
        try (ZipInputStream zis = new ZipInputStream(is))
        {

            byte[] buf = new byte[BUFSIZE];

            while ((entry = zis.getNextEntry()) != null)
            {
                String name = entry.getName();

                long time = entry.getTime();
                time = (time != -1) ? time : new Date().getTime();

                File file = new File(targetDir, name);

                if (entry.isDirectory())
                {
                    boolean created = file.mkdirs();
                    if (!created)
                        throw new IOException("Cannot create the folder named " + file.getName());
                }
                else
                {
                    boolean created = file.getParentFile().mkdirs();
                    if (!created)
                        throw new IOException("Cannot create the folder named " + file.getName());

                    try (FileOutputStream fos = new FileOutputStream(file))
                    {
                        int len;
                        while ((len = zis.read(buf, 0, BUFSIZE)) > 0)
                        {
                            fos.write(buf, 0, len);
                        }
                    }
                }

                boolean settinLastModified = file.setLastModified(time);
                if (!settinLastModified)
                    throw new IOException("Cannot create the folder named " + file.getName());

            }
        }
    }

    /**
     * Added a directory into zip file
     *
     * @param dir - Directory/File to zip, first time should be a directory.
     * @param zip - output stream to write on
     * @param zipFile - Output .zip file
     * @param baseName - This is the absolute path which begin recursive zip (first time is the same as dir.getAbsolutePath()),
     *                 later on is used to retrieve relative path of file inside the zip.
     *
     * @throws IOException if an I/O error has occurred
     */
    public static void addDirToZip(File dir, ZipOutputStream zip, File zipFile, String baseName) throws IOException
    {
        File[] files = dir.listFiles();
        if (files == null)
            return;

        for (File file : files)
        {
            if (file.isDirectory())
            {
                addDirToZip(file, zip, zipFile, baseName);
            }
            else
            {
                String entryName = file.getAbsolutePath().substring(baseName.length());

                if (StringUtils.equals(zipFile.getName(), entryName))
                {
                    continue;
                }
                ZipEntry zipEntry = new ZipEntry(entryName);
                zip.putNextEntry(zipEntry);
                try (FileInputStream fileInput = new FileInputStream(file))
                {
                    IOUtils.copy(fileInput, zip);
                    zip.closeEntry();
                }
            }
        }
    }
}
