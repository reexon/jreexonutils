/*
  Copyright (c) 2016 Marco Velluto
  The code contains extracts of apace library.
  So this code is released under Apace Licence V2 License.
 */
package it.reexon.lib.file;

import it.reexon.lib.secure.algorithmics.MessageDigestAlgorithms;
import org.jetbrains.annotations.NotNull;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageInputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.security.NoSuchAlgorithmException;
import java.util.zip.CRC32;


/**
 * @author Marco Velluto.
 * @since 1.0
 */
@SuppressWarnings({ "WeakerAccess", "unused" })
public class FileUtils
{
    private static final int BUFFER_SIZE = 4 * 1024;
    private static final boolean CLOCK = true;
    private static final boolean VERIFY = true;
    private static final boolean LOGS = true;

    private FileUtils()
    {/* Could not instance it*/}

    /**
     * Utility method to read image from disk and transform image to BufferedImage object 
     *
     * @param is - relative path to the image
     * @param format - file prefix of the image 
     *
     * @since 1.0
     *
     * @return BufferedImage representation of the image 
     */
    public static BufferedImage bitmapToImage(@NotNull InputStream is, @NotNull String format) throws IOException
    {
        final ImageReader rdr = ImageIO.getImageReadersByFormatName(format).next();
        try (final ImageInputStream imageInput = ImageIO.createImageInputStream(is))
        {
            rdr.setInput(imageInput);
            return rdr.read(0);
        }
    }

    /**
     * Check the checksum files with algorithm SHA-256
     *
     * @param firstFile     file ordinal
     * @param secondFile    file to check
     * @return - true if files are equals
     *         - null if there was an error         
     *
     * @since 1.0
     *
     * @throws IOException              If the first byte cannot be read for any reason other than the end of the file, if the input stream has been closed, or if some other I/O error occurs. 
     * @throws IllegalArgumentException If either params is null  
     */
    public static boolean checkEqualDirectory(@NotNull File firstFile, @NotNull File secondFile) throws IOException
    {
        return CheckFilesUtils.checkEqualsDirectories(firstFile, secondFile);
    }

    /**
     * Check the checksum files with algorithm SHA-256
     *
     * @param firstFile     file ordinal
     * @param secondFile    file to check
     * @return - true if files are equals
     *         - null if there was an error         
     *
     * @since 1.0
     *
     * @throws IOException              If the first byte cannot be read for any reason other than the end of the file, if the input stream has been closed, or if some other I/O error occurs. 
     * @throws IllegalArgumentException If either params is null  
     * @throws FileNotFoundException    If file not exists
     */
    public static Boolean checkEqualFiles(@NotNull File firstFile, @NotNull File secondFile) throws IOException
    {
        try
        {
            return CheckFilesUtils.checkEqualsFiles(firstFile, secondFile, MessageDigestAlgorithms.SHA_256);
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new RuntimeException(e);
        }
    }

    /**
     * Utility method for copying directory 
     *
     * @param srcDir - source directory 
     * @param dstDir - destination directory 
     *
     * @since 1.0
     *
     * @throws IllegalArgumentException if srcDir or dstDir are null or is not exists or srcDir is not a directory
     * @throws IOException  If the first byte cannot be read for any reason other than the end of the file, if the input stream has been closed, or if some other I/O error occurs.Author:
     */
    public static void copyDirectory(@NotNull File srcDir, @NotNull File dstDir) throws IOException
    {
        if (!srcDir.exists())
        {
            throw new IllegalArgumentException("SrcDir is not exists");
        }

        if (srcDir.isDirectory())
        {
            if (!dstDir.exists())
            {
                boolean created = dstDir.mkdir();
                if (!created)
                    throw new IOException("Cannot possible create directory " + dstDir.getName());
            }

            String[] list = srcDir.list();
            if (list != null)
                for (String aChildren : list)
                {
                    copyDirectory(new File(srcDir, aChildren), new File(dstDir, aChildren));
                }
        }
        else
        {
            copyFile(srcDir, dstDir);
        }
    }

    /**
     * Utility method for copying file 
     *
     * @param srcFile - source file 
     * @param destFile - destination file 
     * @author A. Weinberger
     *
     * @since 1.0
     *
     * @throws IOException              If the first byte cannot be read for any reason other than the end of the file, if the input stream has been closed, or if some other I/O error occurs.
     * @throws IllegalArgumentException If file are null
     * @throws FileNotFoundException    If srcFile doesn't exist
     */
    public static void copyFile(@NotNull File srcFile, @NotNull File destFile) throws IOException
    {
        if (!srcFile.exists())
        {
            throw new FileNotFoundException("Start file must be exists");
        }

        try (final InputStream in = new FileInputStream(srcFile); final OutputStream out = new FileOutputStream(destFile))
        {
            long millis = System.currentTimeMillis();
            CRC32 checksum;
            if (VERIFY)
            {
                checksum = new CRC32();
                checksum.reset();
            }
            final byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead = in.read(buffer);
            while (bytesRead >= 0)
            {
                if (VERIFY)
                {
                    checksum.update(buffer, 0, bytesRead);
                }
                out.write(buffer, 0, bytesRead);
                bytesRead = in.read(buffer);
            }
            if (CLOCK)
            {
                millis = System.currentTimeMillis() - millis;
                if (LOGS)
                {
                    System.out.println("Copy file '" + srcFile.getPath() + "' on " + millis / 1000L + " second(s)");
                }
            }
        }
    }

    /**
     * Deletes a directory recursively. 
     *
     * @param directory  directory to delete
     *
     * @since 1.0
     *
     * @throws IOException in case deletion is unsuccessful
     * @throws IllegalArgumentException if directory is null
     * @throws FileNotFoundException if directory not exists
     */
    public static void deleteDirectory(@NotNull File directory) throws IOException
    {
        if (!directory.exists())
        {
            throw new FileNotFoundException("The Directory must exists");
        }

        org.apache.commons.io.FileUtils.deleteDirectory(directory);
    }

    /**
     * Deletes a file. 
     *
     * @param file the path to the file 
     * @return boolean
     *
     * @since 1.0
     *
     * @throws IOException if an I/O error occurs
     * @throws IllegalArgumentException if file is null
     */
    public static boolean deleteFile(@NotNull Path file) throws IOException
    {
        try
        {
            if (file.toFile().canWrite())
            {
                Files.delete(file);
            }
            return !Files.exists(file, LinkOption.NOFOLLOW_LINKS);
        }
        catch (IOException e)
        {
            e.printStackTrace();
            throw new IOException("An IO exception occurred while deleting file '" + file + "' with error:" + e.getLocalizedMessage());
        }
    }

    //-----------------------------------------------------------------------

    /**
     * Deletes a file. If file is a directory, delete it and all sub-directories.
     * <p>
     * The difference between File.delete() and this method are:
     * <ul>
     * <li>A directory to be deleted does not have to be empty.</li>
     * <li>You get exceptions when a file or directory cannot be deleted.
     *      (java.io.File methods returns a boolean)</li>
     * </ul>
     *
     * @param file  file or directory to delete, must not be {@code null}
     *
     * @since 1.0
     *
     * @throws IllegalArgumentException if the directory is {@code null}
     * @throws FileNotFoundException if the file was not found
     * @throws IOException in case deletion is unsuccessful
     */
    public static void forceDelete(@NotNull File file) throws IOException
    {
        if (file.isDirectory())
        {
            deleteDirectory(file);
        }
        else
        {
            boolean filePresent = file.exists();
            if (!file.delete())
            {
                if (!filePresent)
                {
                    throw new FileNotFoundException("File does not exist: " + file);
                }
                String message = "Unable to delete file: " + file;
                throw new IOException(message);
            }
        }
    }

    /**
     * Generate byte[] form file
     *
     * @param file to be generate byte[]
     * @return byte[] file
     *
     * @since 1.0
     *
     * @throws IOException                  if for some other reason cannot be opened for reading.
     * @throws IllegalArgumentException     if the file does not exist, is a directory rather than a regular file.
     */
    public static byte[] getByteFromFile(@NotNull File file) throws IOException
    {
        try (ByteArrayOutputStream ous = new ByteArrayOutputStream(); InputStream ios = new FileInputStream(file))
        {
            byte[] buffer = new byte[BUFFER_SIZE];

            int read;
            while ((read = ios.read(buffer)) != -1)
            {
                ous.write(buffer, 0, read);
            }
            return ous.toByteArray();
        }
    }

    /**
     * Convenience method that returns a scaled instance of the provided {@code BufferedImage}. 
     *
     * @param img the original image to be scaled 
     * @param targetWidth the desired width of the scaled instance, in pixels 
     * @param targetHeight the desired height of the scaled instance, in pixels 
     * @param hint one of the rendering hints that corresponds to {@code RenderingHints.KEY_INTERPOLATION} (e.g. 
     *        {@code RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR}, {@code RenderingHints.VALUE_INTERPOLATION_BILINEAR}, 
     *        {@code RenderingHints.VALUE_INTERPOLATION_BICUBIC}) 
     * @param higherQuality if true, this method will use a multi-step scaling technique that provides higher quality than the 
     *        usual one-step technique (only useful in downscaling cases, where {@code targetWidth} or {@code targetHeight} is 
     *        smaller than the original dimensions, and generally only when the {@code BILINEAR} hint is specified) 
     * @return a scaled version of the original {@code BufferedImage} 
     *
     * @since 1.0
     */
    public static BufferedImage getScaledInstance(@NotNull BufferedImage img, int targetWidth, int targetHeight, @NotNull Object hint,
                                                  boolean higherQuality)
    {
        final int type = img.getTransparency() == Transparency.OPAQUE ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
        BufferedImage ret = img;
        int w;
        int h;
        if (higherQuality)
        {
            // Use multi-step technique: start with original size, then 
            // scale down in multiple passes with drawImage() 
            // until the target size is reached 
            w = img.getWidth();
            h = img.getHeight();
        }
        else
        {
            // Use one-step technique: scale directly from original 
            // size to target size with a single drawImage() call 
            w = targetWidth;
            h = targetHeight;
        }

        do
        {
            if (higherQuality && w > targetWidth)
            {
                w /= 2;
                if (w < targetWidth)
                {
                    w = targetWidth;
                }
            }

            if (higherQuality && h > targetHeight)
            {
                h /= 2;
                if (h < targetHeight)
                {
                    h = targetHeight;
                }
            }

            final BufferedImage tmp = new BufferedImage(w, h, type);
            final Graphics2D g2 = tmp.createGraphics();
            g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, hint);
            g2.drawImage(ret, 0, 0, w, h, null);
            g2.dispose();

            ret = tmp;
        }
        while (w != targetWidth || h != targetHeight);

        return ret;
    }

    /**
     * Utility method to write BufferedImage object to disk
     *
     * @param image - BufferedImage object to save.
     * @param data - relative path to the image
     * @param format - file prefix of the image
     *
     * @return BufferedImage representation of the image
     *
     * @since 1.0
     */
    public static void imageToBitmap(@NotNull BufferedImage image, @NotNull String data, @NotNull String format) throws IOException
    {
        try (final OutputStream inb = new FileOutputStream(data); final ImageInputStream imageInput = ImageIO.createImageOutputStream(inb))
        {
            final ImageWriter wrt = ImageIO.getImageWritersByFormatName(format).next();
            wrt.setOutput(imageInput);
            wrt.write(image);
        }
    }

    /**
     * Moves a file to a destination. 
     *
     * @param file the path to the file to move 
     * @param destination the destination path 
     *
     * @since 1.0
     *
     * @throws IOException  if an I/O error occurs
     * @throws IllegalArgumentException if file is null or not exists, destination is null
     */
    public static void moveFile(@NotNull Path file, @NotNull Path destination) throws IOException, IllegalArgumentException
    {
        if (!Files.exists(file, LinkOption.NOFOLLOW_LINKS))
        {
            throw new IllegalArgumentException("The filepath is null or points to an invalid location! " + file);
        }

        Files.move(file, destination, StandardCopyOption.REPLACE_EXISTING);
    }

}
