package it.reexon.lib.file;

/*
  Copyright (c) 2016 Marco Velluto

  Licensed to the Apache Software Foundation (ASF) under one or more
  contributor license agreements.  See the NOTICE file distributed with
  this work for additional information regarding copyright ownership.
  The ASF licenses this file to You under the Apache License, Version 2.0
  (the "License"); you may not use this file except in compliance with
  the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

import it.reexon.lib.secure.algorithmics.MessageDigestAlgorithms;
import it.reexon.lib.secure.checksum.ChecksumUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.*;


/**
 * Checks if the two files are equal. 
 * Also check if either file has been corrupted . 
 *
 * @author Marco Velluto
 * @since 1.0
 */
@SuppressWarnings("WeakerAccess")
public class CheckFilesUtils
{
    private static final Logger logger = LogManager.getLogger(CheckFilesUtils.class);

    private CheckFilesUtils()
    {/* Could not instance it*/}

    /**
     * Checks the checksum files with algorithm
     *
     * @param firstFile     file original
     * @param secondFile    file to check
     * @param algorithm     algorithm to use. 
     *
     * @return true                     If files are equals
     *
     * @throws IOException              If the first byte cannot be read for any reason other than the end of the file, if the input stream has been closed, or if some other I/O error occurs.
     * @throws NoSuchAlgorithmException If no Provider supports a MessageDigestSpi implementation for the specified algorithm.
     * @throws IllegalArgumentException If either params is null  
     * @throws FileNotFoundException    If file not exists
     */
    public static boolean checkEqualsFiles(@NotNull File firstFile, @NotNull File secondFile, @NotNull String algorithm)
            throws IOException, NoSuchAlgorithmException
    {
        String checksumFirst = ChecksumUtils.getChecksum(firstFile, algorithm);
        String checksumSecond = ChecksumUtils.getChecksum(secondFile, algorithm);
        return StringUtils.equals(checksumFirst, checksumSecond);
    }

    /**
     * Checks if the folders are the same and if they contain the same files if two directory are equals and content
     *
     * @param firstDirectory    first directory to check
     * @param secondDirectory   second directory to check
     * @return - true if directories are equals
     *
     * @throws IOException              If the first byte cannot be read for any reason other than the end of the file, if the input stream has been closed, or if some other I/O error occurs.
     * @throws IllegalArgumentException If either params is null
     */
    public static boolean checkEqualsDirectories(@NotNull File firstDirectory, @NotNull File secondDirectory) throws IOException
    {
        if (!firstDirectory.isDirectory() || !secondDirectory.isDirectory())
        {
            return false;
        }

        File[] firstFilesArrays = firstDirectory.listFiles();
        File[] secondFilesArrays = secondDirectory.listFiles();

        List<File> firstFiles = firstFilesArrays == null ? Collections.emptyList() : Arrays.asList(firstFilesArrays);
        List<File> secondFiles = secondFilesArrays == null ? Collections.emptyList() : Arrays.asList(secondFilesArrays);

        if (firstFiles.size() != secondFiles.size())
        {
            return false;
        }

        for (File firstFile : firstFiles)
        {
            Boolean isEquals = Boolean.FALSE;
            for (File secondFile : secondFiles)
            {
                try
                {
                    isEquals = checkEqualsFiles(firstFile, secondFile, MessageDigestAlgorithms.getDefault());
                }
                catch (NoSuchAlgorithmException e)
                {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
                if (isEquals)
                {
                    break;
                }
            }
            if (!isEquals)
            {
                return false;
            }
        }
        return true;
    }

    /**
     *
     * Check if the same files are present by name
     *
     * @param files1 1° list
     * @param files2 2° list
     *
     * @return true if all files are equals.
     */
    public static boolean checkFilesForName(@NotNull Collection<File> files1, @NotNull Collection<File> files2)
    {
        boolean isEquals = true;
        for (File file1 : files1)
        {
            Optional<File> fileOptional = files2.parallelStream().filter(p -> p.getName().equals(file1.getName())).findAny();
            if (fileOptional.isPresent())
            {
                if (file1.isDirectory())
                {
                    File[] filesArray = file1.listFiles();
                    File[] files2Array = fileOptional.get().listFiles();
                    if (filesArray == null || files2Array == null)
                        continue;
                    isEquals = checkFilesForName(Arrays.asList(filesArray), Arrays.asList(files2Array));
                }
            }
            else
            {
                if (file1.isFile())
                {
                    logger.warn("File {} not found", file1.getName());
                }
                else if (file1.isDirectory())
                {
                    logger.warn("Directory {} not found", file1.getName());
                }

                isEquals = false;
            }
        }
        return isEquals;
    }

}
