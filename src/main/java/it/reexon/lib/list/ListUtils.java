/*
  Copyright (c) 2016 Marco Velluto
 */
package it.reexon.lib.list;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.stream.Collectors;


/**
 * @author Marco Velluto
 * @since 1.0
 */
@SuppressWarnings("WeakerAccess")
public class ListUtils
{
    private ListUtils()
    {}

    /**
     * <pre>
     * This function resolve the warning:
     *          The expression of type List needs unchecked conversion to conform to List
     * Example: 
     *          List<{Object}> findAllById(Long id)
     *          ...
     *          return castList(AnagSoggetto.class, query.list());
     * </pre>    
     * @param clazz     class type
     * @param c         list 
     * @return list
     *
     * @since 1.0
     */
    public static <T> List<T> castList(@NotNull Class<? extends T> clazz, @NotNull Collection<?> c)
    {
        List<T> r = new ArrayList<>(c.size());
        for (Object o : c)
            r.add(clazz.cast(o));
        return r;
    }

    /**
     * Find Average Element Integer from List
     * Example: 
     * <code>   List<Int> = {1, 2, 3}
     *          return 2
     * </code>        
     * @param list list to find average element
     *
     * @return Average Element.
     *          Null if list is empty
     *
     * @since 1.0
     *
     * @throws IllegalArgumentException if list param is null
     */
    @Nullable
    @SuppressWarnings("ConstantConditions")
    public static Integer findAverageElementInt(@NotNull List<? extends Integer> list)
    {
        if (list.isEmpty())
        {
            return null;
        }

        Double averageDouble = list.stream().mapToInt(i -> i).average().getAsDouble();
        int average = averageDouble.intValue();

        return sortListByElement(new ArrayList<>(list.stream().map(Integer::longValue).collect(Collectors.toList())), (long) average).stream()
                                                                                                                                     .findFirst()
                                                                                                                                     .get()
                                                                                                                                     .intValue();
    }

    /**
     * Find Average Element Long from List
     *
     * @param list list to find average element
     * @return  <ul>Average Element.
     *          <ul>Null if list is empty
     * <pre>
     * Example:
     *          List<Long> = {1L, 2L, 3L}
     *          return 2L
     * </pre>
     *
     * @since 1.0
     *
     * @throws IllegalArgumentException if list is empty
     */
    @NotNull
    @SuppressWarnings("ConstantConditions")
    public static Long findAverageElementLong(@NotNull List<? extends Long> list)
    {
        if (list.isEmpty())
        {
            throw new IllegalArgumentException("The list cannot be null!");
        }

        Double averageDouble = list.stream().mapToLong(i -> i).average().getAsDouble();
        long average = averageDouble.longValue();

        return sortListByElement(list, average).stream().findFirst().get();
    }

    /**
     * Order Long list by an element
     * <pre>
     * Example: 
     * <br>    list = {1,2,3}
     * <br>    element = {2}
     * <br>    return = {2,1,3}
     * </pre>
     *
     * @param list list to order
     * @param element element in which to sort
     *
     * @return sorted list;
     *          null if list is empty
     *
     * @since 1.0
     *
     * @throws IllegalArgumentException if list is null or element is null
     */
    public static List<Long> orderByElementLong(@NotNull final Collection<? extends Long> list, @NotNull Long element)
    {
        if (list.isEmpty())
        {
            return Collections.emptyList();
        }

        return sortListByElement(list, element);
    }

    /**
     * It takes the list item that is closest to the value passed as a parameter.
     *
     * @param list      list from which you find the value
     * @param element   element to look for
     *
     * @return item that is closest to the value passed as a parameter.
     *
     * @since 1.0
     *
     * @throws IllegalArgumentException - if list is null or element is null
     */
    @SuppressWarnings("ConstantConditions")
    @NotNull
    public static Long nearestElement(@NotNull List<? extends Long> list, @NotNull Long element)
    {
        List<Long> sortedList = orderByElementLong(list, element);
        if (sortedList.isEmpty())
        {
            throw new IllegalArgumentException("The list cannot be empty");
        }
        return sortedList.stream().findFirst().get();
    }

    /**
     * Create a List form elements
     *
     * @param element array element to add
     *
     * @since 1.0
     *
     * @return - list with elements
     */
    @NotNull
    @SafeVarargs
    public static <T> List<T> createList(T... element)
    {
        return new LinkedList<>(Arrays.asList(element));
    }

    private static List<Long> sortListByElement(final Collection<? extends Long> list, Long element)
    {
        List<Long> listToSort = new ArrayList<>(list);
        listToSort.sort((p1, p2) ->
                        {
                            long distance1 = p1 - element;
                            long distance2 = p2 - element;

                            return Long.compare(distance1 < 0 ? distance1 * (-1) : distance1, distance2 < 0 ? distance2 * (-1) : distance2);
                        });

        return listToSort;
    }

}
