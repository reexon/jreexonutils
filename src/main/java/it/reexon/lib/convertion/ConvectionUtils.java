package it.reexon.lib.convertion;

import org.jetbrains.annotations.NotNull;


/**
 * Created by Marco Velluto on 06/04/2017.
 *
 *  @since 1.0
 */
@SuppressWarnings({ "WeakerAccess" })
public class ConvectionUtils
{
    private ConvectionUtils()
    {/* Could not instance it*/}

    /**
     * Convert Byte in Mega Byte
     *
     * @param byteValue byte value to convert
     *
     * @return the Mega Byte value
     *
     * @since 1.0
     */
    @NotNull
    public static Number convertByteInMB(@NotNull Number byteValue)
    {
        return (byteValue.doubleValue() / 1024) / 1024;
    }
}
