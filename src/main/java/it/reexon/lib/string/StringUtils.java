package it.reexon.lib.string;

import org.jetbrains.annotations.NotNull;


@SuppressWarnings("unused")
public class StringUtils
{
    /**
     * How-to for a faster way to convert
     * a byte array to a HEX string
     *
     * @param byteArray hex byte[] to covert in string
     * @return string converted
     *
     * @since 1.0
     */
    @NotNull
    public static String toHexString(@NotNull byte[] byteArray)
    {
        StringBuilder sb = new StringBuilder();
        for (byte aByteArray : byteArray)
        {
            sb.append(Integer.toString((aByteArray & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }

    /**
     * Convert string in hex characters
     *
     * @param str String to convert
     * @return byte[] of characters
     *
     * @since 1.0
     */
    public static byte[] toHex(@NotNull String str)
    {
        byte[] bytes = new byte[str.length() / 2];
        for (int i = 0; i < bytes.length; i++)
        {
            bytes[i] = (byte) Integer.parseInt(str.substring(2 * i, 2 * i + 2), 16);
        }
        return bytes;
    }
}
