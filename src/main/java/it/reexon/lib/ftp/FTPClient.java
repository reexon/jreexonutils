package it.reexon.lib.ftp;

import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPFileFilter;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * @author Loris D'Antonio
 */
@SuppressWarnings("WeakerAccess")
public class FTPClient extends org.apache.commons.net.ftp.FTPClient
{

    /**
     * Basically this function is used to start a recursive search on FTP Server with FTPFileFilter.
     *
     * @param startPath Path to start recursive search
     * @param filter    FTPFileFilter
     *
     * @return List<FTPFile>
     *
     * @throws IOException If some other I/O error occurs
     */
    public List<FTPFile> deepSearch(@NotNull String startPath, FTPFileFilter filter) throws IOException
    {

        FTPFile[] files = listFiles(startPath.isEmpty() ? "/" : startPath);
        List<FTPFile> listMatchingFiles = new ArrayList<>();

        for (FTPFile file : files)
        {
            if (file.isDirectory())
            {
                listMatchingFiles.addAll(deepSearch(startPath + '/' + file.getName(), filter));
            }

            else if (file.isFile() && filter.accept(file))
            {
                listMatchingFiles.addAll(new ArrayList<>(Collections.singletonList(file)));
            }

        }

        return listMatchingFiles;
    }
}
