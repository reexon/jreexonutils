package it.reexon.lib.ftp;

import org.apache.commons.net.ftp.FTPFileFilter;
import org.jetbrains.annotations.NotNull;


/**
 * @author Loris D'Antonio
 */
@SuppressWarnings({ "SameParameterValue", "WeakerAccess" })
public class FTPFileFilters extends org.apache.commons.net.ftp.FTPFileFilters
{

    /**
     * @param format - extension to filter (with dot) (.txt,.mkv,.mp3,etc)
     *
     * @return FTPFileFilter
     */
    public static FTPFileFilter FORMAT(@NotNull String format)
    {
        return file -> file.getName().toLowerCase().endsWith(format);
    }

    /**
     * @param regexp - name of file/directory to find, can be used with regex ex(.*filename.*)
     *
     * @return FTPFileFilter
     */
    public static FTPFileFilter REGEXP(@NotNull String regexp)
    {
        return file -> file.getName().toLowerCase().matches(regexp);
    }
}
