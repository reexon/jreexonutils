/*
  Copyright (c) 2016 Marco Velluto. All rights reserved.
 */
package it.reexon.lib.manipulation;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


/**
 * @author Marco Velluto
 * @since 1.0
 */
@SuppressWarnings("unused")
public class ManipulationUtils
{

    private ManipulationUtils()
    {/* Could not instance it*/}

    /**
     * Order randomly the characters of a String
     *
     * <pre>
     * Example:
     *  shuffle("Hello") = hlleo
     *  shuffle("Hello") = llheo
     *  shuffle("Hello") = leohl
     *  shuffle("Hello") = lleho
     * </pre>
     * @param input String to be sorted randomly
     * @throws IllegalArgumentException if input string is null
     *
     * @since 1.0
     */
    @NotNull
    public static String shuffle(@NotNull String input)
    {
        List<Character> characters = new ArrayList<>();
        for (char c : input.toCharArray())
        {
            characters.add(c);
        }
        StringBuilder output = new StringBuilder(input.length());
        while (characters.size() != 0)
        {
            int randPicker = (int) (Math.random() * characters.size());
            output.append(characters.remove(randPicker));
        }
        return output.toString();
    }

    /**
     * Generate a permutation (n!) from string
     * The factorial of a non-negative integer n, denoted by n!, is the product of all positive integers less than or equal to n. 
     * n! = n*(n-1)*(n-2)...1
     * <pre>
     * For example:
     * 5! = 5*4*3*2*1 = 120
     *
     * ABCD =   ABCD BACD CABD DABC
     *          ABDC BADC CADB DACB
     *          ACBD BCAD CBAD DBAC
     *          ACDB BCDA CBDA DBCA
     *          ADBC BDAC CDAB DCAB
     *          ADCB BDCA CDBA DCBA
     *
     * The value of 0! is 1, according to the convention for an empty product.
     * </pre>
     * @param string       String to permutation
     *
     * @return List<String> permutation
     *
     * @since 1.0
     */
    public static List<String> permutatios(@NotNull String string)
    {
        List<String> strings = new LinkedList<>();
        char[] arrayStr = string.toCharArray();
        for (int i = 0; i < string.length(); i++)
        {
            for (int j = 0; j < string.length(); j++)
            {
                char iChar = arrayStr[i];
                char jChar = arrayStr[j];
                arrayStr[j] = iChar;
                arrayStr[i] = jChar;
                strings.add(new String(arrayStr));
            }
        }
        return strings;
    }

    /**
     * Generate a permutation (n!) from array element
     * The factorial of a non-negative integer n, denoted by n!, is the product of all positive integers less than or equal to n. 
     * <pre>
     * n! = n*(n-1)*(n-2)...1
     *
     * For example:
     * 5! = 5*4*3*2*1 = 120
     *
     * ABCD =   ABCD BACD CABD DABC
     *          ABDC BADC CADB DACB
     *          ACBD BCAD CBAD DBAC
     *          ACDB BCDA CBDA DBCA
     *          ADBC BDAC CDAB DCAB
     *          ADCB BDCA CDBA DCBA
     *
     * The value of 0! is 1, according to the convention for an empty product.
     * </pre>
     *
     * @return List<String> permutation
     *
     * @since 1.0
     */
    public static <E> List<List<E>> generatePermutations(List<E> original)
    {
        if (original.size() == 0)
        {
            List<List<E>> result = new ArrayList<>();
            result.add(new ArrayList<>());
            return result;
        }
        E firstElement = original.remove(0);
        List<List<E>> returnValue = new ArrayList<>();
        List<List<E>> permutations = generatePermutations(original);
        for (List<E> smallerPermutated : permutations)
        {
            for (int index = 0; index <= smallerPermutated.size(); index++)
            {
                List<E> temp = new ArrayList<>(smallerPermutated);
                temp.add(index, firstElement);
                returnValue.add(temp);
            }
        }
        return returnValue;
    }

    /**
     * Returns the maximum number of permutations.
     *
     * <pre>
     * n! = n*(n-1)*(n-2)...1
     *
     * For example:
     * 5! = 5*4*3*2*1 = 120
     * </pre>
     *
     * @param number     number of permutation
     *
     * @return the maximum number of permutations
     *
     * @since 1.0
     */
    public static long permutatiosCount(int number)
    {
        if (number < 0)
        {
            throw new IllegalArgumentException("The number must be greater than 0");
        }

        if (number == 0)
        {
            return 1;
        }
        else
        {
            return number * permutatiosCount(number - 1);
        }
    }

    //TODO: Sviluppare Insiemi con ripetizioni ovvero coefficiente multinomiale.
    //TODO: Sviluppare Coefficiente Binomiale.
}
